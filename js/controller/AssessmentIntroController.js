app.controller('AssessmentIntroController', function($scope,$http,global,$rootScope){
    $scope.assess_details = global.assessment_intro;
    $scope.title = $scope.assess_details['title'];
    $scope.assessmentid = function(id,status){
        $("#assessment_status").val(status);
        $("#assessment_id").val(id);
        window.location = "#previewassessment";
    }
});