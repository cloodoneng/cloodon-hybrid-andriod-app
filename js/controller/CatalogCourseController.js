app.controller('CatalogCourseController',function($scope,$http,$rootScope,CommonService,$ionicHistory,logindetailservice){
    $scope.settings = {};
    $scope.settings['Tab1'] = true;
    $scope.settings['Tab2'] = false;
    $scope.settings['Tab3'] = false;
    if(CommonService.course_dict){
        $scope.course = CommonService.course_dict;
        $rootScope.title = $scope.course['course_name']
    }
    $scope.enroll = function(enroll_url){
        url = ajax_domain+enroll_url;
        $rootScope.enrollment_url = url;
        window.location="#enroll"
    }
    $scope.tabbed = function(tab){
        $scope[tab] = true;
        angular.forEach($scope.settings, function(value, key) {
            if(key == tab){
                $scope.settings[key] = true;
            }
            else{
                $scope.settings[key] = false;
            }
        });
    }
})