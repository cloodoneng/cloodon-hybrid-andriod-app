app.controller('CatalogCourseListController',function($scope,global,$http,$rootScope,CommonService,$stateParams){
    $scope.title = $stateParams.course_name
    $scope.courses = CommonService.catalog_course_list;
    $scope.course_detail = function(course){
        CommonService.get_catalog_course(course);
        window.location = "#course"
    }
})