app.controller('CourseDetailController',function($scope,global,$http,$rootScope,coursedetailservice,ombookservice,$q,$interval,$ionicHistory,$state){
    $scope.batch_id = global.batch_id;
    $scope.title = global.course_title;
    var connection;
    $scope.settings = {};
    $scope.settings['Tab1'] = true;
    $scope.settings['Tab2'] = false;
    $scope.settings['Tab3'] = false;
    var promise = coursedetailservice.get_course_detail($scope.batch_id)
    promise.then(function(data) {
        if(data['response']){
            $scope.ass_detail(data['response']['assessments']);
            $scope.discussion_view(data['response']['discussions']);
            $scope.omb(data['response']['ombook_list']);
        }
    }, function(reason) {
        console.log('Failed: ' + reason);
    }, function(update) {
        console.log('Got notification: ' + update);
    });
    $scope.omb = function(data){
        if(data.length >= 1){
            var promise = ombookservice.get_ombook($scope.batch_id,data[0].id,data[0].version)
            promise.then(function(data) {
                if(data['ombook']){
                    $scope.ombook_view(data['ombook']);
                }
            }, function(reason) {
                console.log('Failed: ' + reason);
            }, function(update) {
                console.log('Got notification: ' + update);
            });
        }
        else{
            $scope.ombook_view('No ombook');
        }
    }
    $scope.ombook_view = function(data){
        if(data == 'No ombook'){
            $scope.ombook = 'None'
        }
        else{
            p_no = 0;
            $scope.ombook = data;
            var gate_cleared = true;
            for(i=0;i<$scope.ombook['chapters'].length;i++){
                if($scope.ombook['chapters'][i]['desc'] != ''){
                    p_no = p_no +1;
                    $scope.ombook['chapters'][i]['c_p_no'] = p_no;
                }
                if(i!=0){
                    if($scope.ombook['chapters'][i]['gate_questions']){
                        gate_cleared = false;
                        for(clrg=0;clrg<$scope.ombook['cleared_gates'].length;clrg++){
                            if($scope.ombook['chapters'][i-1]['cid'] == $scope.ombook['cleared_gates'][clrg]['cid']){
                                if($scope.ombook['cleared_gates'][clrg]['cleared'] == 1){
                                    gate_cleared = true;
                                }
                            }
                        }
                    }
                    $scope.ombook['chapters'][i]['prev_chap_clear'] = gate_cleared;
                }
                for(cg=0;cg<$scope.ombook['cleared_gates'].length;cg++){
                    if($scope.ombook['chapters'][i]['cid'] == $scope.ombook['cleared_gates'][cg]['cid']){
                        $scope.ombook['chapters'][i]['cleared'] = $scope.ombook['cleared_gates'][cg]['cleared'];
                        $scope.ombook['chapters'][i]['best_score'] = $scope.ombook['cleared_gates'][cg]['best_score']
                    }
                }
                if(i!=0){
                    if($scope.ombook['chapters'][i-1]['gate_questions'] != false && $scope.ombook['chapters'][i-1]['cleared'] == 1 && ($scope.ombook['chapters'][i-1]['prev'] == undefined || $scope.ombook['chapters'][i-1]['prev'] == true)){
                        $scope.ombook['chapters'][i]['prev'] = true;
                    }
                    else if(($scope.ombook['chapters'][i-1]['gate_questions'] != false && ($scope.ombook['chapters'][i-1]['cleared'] == 0 || $scope.ombook['chapters'][i-1]['cleared'] == undefined)) && ($scope.ombook['chapters'][i-1]['prev'] == undefined || $scope.ombook['chapters'][i-1]['prev'] == true)){
                        $scope.ombook['chapters'][i]['prev'] = false;
                    }
                    else if($scope.ombook['chapters'][i-1]['prev'] == false){
                        $scope.ombook['chapters'][i]['prev'] = false;
                    }
                    else{
                        $scope.ombook['chapters'][i]['prev'] = true;
                    }
                }
                if($scope.ombook['chapters'][i]['lpages']){
                    for(l=0;l<$scope.ombook['chapters'][i]['lpages'].length;l++){
                        p_no = p_no +1;
                        $scope.ombook['chapters'][i]['lpages'][l]['p_no'] = p_no;
                        pid = $scope.ombook['chapters'][i]['lpages'][l]['pid']
                        if($scope.ombook['chapters'][i]['lpages'][l]['ttype'] == '2'){
                            for(op=0;op<$scope.ombook['chapters'][i]['lpages'][l]['qjson']['options'].length;op++){
                                $scope.ombook['chapters'][i]['lpages'][l]['qjson']['options'][op]['student_ans'] = false;
                            }
                        }
                        else{

                        }
                        if($scope.ombook['progress'].hasOwnProperty(pid)){
                            $scope.ombook['chapters'][i]['lpages'][l]['progress'] = $scope.ombook['progress'][pid];
                        }
                    }
                }
                if($scope.ombook['chapters'][i]['gate_questions'] != false){
                    p_no = p_no +1;
                    $scope.ombook['chapters'][i]['c_q_no'] = p_no;
                }
                chap_key = "c"+$scope.ombook['chapters'][i]['cid'];
                quiz_key = "q"+$scope.ombook['chapters'][i]['cid'];
                if($scope.ombook['progress'].hasOwnProperty(chap_key)){
                    $scope.ombook['chapters'][i]['chap_progress'] = $scope.ombook['progress'][chap_key];
                }
                if($scope.ombook['progress'].hasOwnProperty(quiz_key)){
                    $scope.ombook['chapters'][i]['quiz_progress'] = $scope.ombook['progress'][quiz_key];
                }
            }
            coursedetailservice.ombook_data['ombook_detail'] = $scope.ombook;
        }
    }
    $scope.discussion_view = function(data){
        angular.forEach(data, function(value, key) {
            value['discussion_thread']['limit_value'] = 0
        });
        $scope.discussions = data;
    }
    $scope.ass_detail = function(ass_list){
        $scope.assessment_list = ass_list
        $scope.ass_list = ass_list;
    }
    $scope.reply = function(dis_id){
        coursedetailservice.discussions = dis_id;
        window.location = "#discussion-detail";
    }
    $scope.discussion = function(){
        var d_id = $("#discussion_id").val()
        var promise = coursedetailservice.get_discussion($scope.batch_id,d_id)
        promise.then(function(response) {
            if(response['discussion'].length >= 1 ){
                angular.forEach(response['discussion'], function(value, key) {
                    value['discussion_thread']['limit_value'] = 0;
                    $scope.discussions.push(value);
                })
            }
            else{
                $("#more-discussion").hide()
                $("#discussion-msg").show()
                $scope.discussion_text = "No more discussions";
            }
        }, function(reason) {
            console.log('Failed: ' + reason);
            $scope.discussion_text = "Check Internet Connectivity";
            $("#discussion-msg").show()
        }, function(update) {
            console.log('Got notification: ' + update);
        });

    }
    $scope.comment= function(id){
        $("#post-comment-"+id).show();
    }
    $scope.submit_comment = function(id){
        $scope.comment['d_id'] = id
        $.ajax({
            type: "POST",
            url: ajax_domain+'/lms/admin/discussion_reply/',
            data: $scope.comment,
            dataType:'json',
            timeout : 70000,
            error : function(){
                alert("check internet connection")
            },
            success : function(response){
               $scope.comment = {};
               //response['reply']['reply_msg'] = $scope.comment['reply']
               for(var i=0; i< $scope.discussions.length; i++){
                   if($scope.discussions[i]['discussion_thread']['id'] == id){
                        $scope.discussions[i]['replies'].push(response['reply'])
                        $scope.discussions[i]['discussion_thread']['limit_value'] = $scope.discussions[i]['replies'].length
                        break;
                   }
               }
            }
        })
    }
	$scope.assessmentdetail = function(id){
        var data = $scope.assessment_list;
        var ass_info = {};
  		for(i=0; i < data.length; i++){
            if(data[i]['id'] == id){
               // var setting = {};
//                setting['show_hint'] = data[i]['settings']['21']
//                setting['shuffle_question'] = data[i]['settings']['22']
//                setting['shuffle_answer'] = data[i]['settings']['23']
//                setting['time_limit'] = parseInt(data[i]['settings']['25'])
//                setting['attempts_allowed'] = data[i]['settings']['26']
//                setting['grading_scheme'] = data[i]['settings']['27']
//                setting['allowed_partial_credit'] = data[i]['settings']['28']
//                setting['save_and_resume'] = data[i]['settings']['30']
//                setting['multi_attempt'] = data[i]['settings']['38']
//                setting['submission_view'] = data[i]['settings']['39']
//                setting['show_submission'] = data[i]['settings']['40']
//                console.log(JSON.stringify(setting))
//                ass_info = data[i];
//                console.log(JSON.stringify(ass_info))
//                ass_info['settings'] = setting;
//                console.log(JSON.stringify(ass_info))
//                //data[i]['settings'] = settings;
//                console.log(JSON.stringify($scope.assessment_list))
  			    global.assessment_intro = data[i];
  			    window.location = "#assessmentintro"
  			}
  			else{
            }
	    };
    }
    $scope.submit_discussion = function(){
        config = []
        config.push($scope.thread)
        $scope.thread['b_id'] = $scope.batch_id
        $.ajax({
            type: "POST",
            url: ajax_domain+'/lms/admin/start_discussion/',
            data: $scope.thread,
            dataType:'json',
            timeout : 70000,
            error : function(){
                alert("check internet connection")
            },
            success : function(response){
                $("#add-discussion").hide();
                var promise = coursedetailservice.get_discussion($scope.batch_id)
                promise.then(function(response) {
                    if(response['status'] == 1){
                        $scope.discussion_view(response['discussion']);
                        $scope.thread = {};
                    }
                }, function(reason) {
                    console.log('Failed: ' + reason);
                }, function(update) {
                    console.log('Got notification: ' + update);
                });
            }
        })
    }
    $scope.ombook_detailed_view = function(cid,pid){
        $state.go('ombook', {cid: cid,pid:pid});
        //window.location='#/ombook/'+cid+'/'+pid;
    }
    $scope.add_discussion = function(){
        $("#add-discussion").show();
    }
    $scope.tabbed = function(tab){
        $scope[tab] = true;
        angular.forEach($scope.settings, function(value, key) {
            if(key == tab){
                $scope.settings[key] = true;
            }
            else{
                $scope.settings[key] = false;
            }
        });
    }
    $scope.$on('$ionicView.enter', function (event, viewData) {
        if($rootScope.reload_course_detail == true){
            var promise = coursedetailservice.get_course_detail($scope.batch_id)
            promise.then(function(data) {
                if(data['response']){
                    $scope.ass_detail(data['response']['assessments']);
                    $scope.discussion_view(data['response']['discussions']);
                    $scope.omb(data['response']['ombook_list']);
                }
            }, function(reason) {
                console.log('Failed: ' + reason);
            }, function(update) {
                console.log('Got notification: ' + update);
            });
        }
        else{

        }
        if($rootScope.update_discussion == 'true'){
            var promise = coursedetailservice.get_discussion($scope.batch_id);
            promise.then(function(data) {
                if(data['status'] == 1){
                angular.forEach(data['discussion'], function(value, key) {
                    value['discussion_thread']['limit_value'] = 0
                });
                    $scope.discussions = data['discussion']
                }
            }, function(reason) {
                console.log('Failed: ' + reason);
            }, function(update) {
                console.log('Got notification: ' + update);
            });
            $rootScope.update_discussion = 'false'
        }
    });
    /*
    $scope.historyGoBack = function(){
        if($rootScope.remove_back_view == true){
            $rootScope.remove_back_view == false;
            //$ionicHistory.goback(-2);
            $scope.history = $ionicHistory.viewHistory()
            $scope.back_value = $scope.history['histories']['root']['stack'].length -1;
            $ionicHistory.goBack(-$scope.back_value);
        }
        else{
             $ionicHistory.goBack();
        }
    }*/
});