app.controller('CourseListController',function($scope,global,$http,$rootScope,$ionicPopover){
    $scope.active = false;
    $scope.courses = global.course_list;
    angular.forEach($scope.courses, function(value, key) {
        value['end_date_obj'] = new Date(value['end_date'])
        value['start_date_obj'] = new Date(value['start_date'])
    });
    console.log(JSON.stringify($scope.courses))
    $scope.searchText = "Live";
    $scope.batchdetail = function(batch_id,course_title){
        global.batch_id = batch_id;
        global.course_title = course_title;
        window.location = "#course-detail";
    }
    var template = '<ion-popover-view style="height:auto"><ion-content style="position:relative"><div class="list" ng-click="popover.hide()"><a class="item" ng-click="sort('+"'Live'"+')">Live</a>'+'<a class="item" ng-click="sort('+"'Open'"+')">Open</a>'+'<a class="item" ng-click="sort('+"''"+')">All</a>'+'</div></ion-content></ion-popover>';
    $scope.popover = $ionicPopover.fromTemplate(template, {
        scope: $scope
    });
    $scope.sort = function(state){
        $("#error_msg").hide()
        $scope.searchText = state;
        $scope.$apply();
        if(($('#filter a')).length == 0){
            $("#error_msg").show()
        }
    }
    $("#error_msg").hide()
    if(($('#filter a')).length == 0){
       $scope.active = true;
    }
    else{
        $scope.active = false;
    }
})