app.controller('CreateDiscussionController',function($scope,global,$http,$rootScope,coursedetailservice,$ionicHistory){
    $scope.batch_id = global.batch_id;
    $scope.thread = {};
    console.log($scope.thread)
    $scope.submit_discussion = function(){
        config = []
        config.push($scope.thread)
        if($scope.thread['discussion_title'] == undefined){
            $("#discussion_title").addClass('has-error bottom-margin')
            console.log("ERROR in title")
        }
        if($scope.thread['discussion_thread'] == undefined){
            $("#discussion_thread").addClass('has-error')
            console.log("ERROR in thread ")
        }
        if($scope.thread['discussion_title'] == undefined || $scope.thread['discussion_thread'] == undefined){
            return;
        }
        $scope.thread['b_id'] = $scope.batch_id
        $.ajax({
            type: "POST",
            url: ajax_domain+'/lms/admin/start_discussion/',
            data: $scope.thread,
            dataType:'json',
            timeout : 70000,
            error : function(){
                alert("check internet connection")
            },
            success : function(response){
                $("#discussion_title").removeClass('has-error bottom-margin')
                $("#discussion_thread").removeClass('has-error')
                $rootScope.update_discussion = 'true'
                $ionicHistory.goBack()
            }
        })
    }
})