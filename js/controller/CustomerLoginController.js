app.controller('CustomerLoginController', function($scope, $ionicHistory, $ionicLoading, $ionicPopup) {
	$scope.customer = {};
	$scope.error_msg = "";
    $scope.submit = function(){
    	$ionicLoading.show();
    	$scope.error_msg = "";
       	var data = $scope.customer;
       	$.ajax({
      	    type: 'POST',
      		url:'http://192.168.254.46/apis/get-customer-data/',
      		crossDomain: true,
       		data: data,
       		dataType: 'jsonp',
       		success: function(response, textStatus, jqXHR) {
       			$ionicLoading.hide();
       			var organization = response['organization_name'];
       			if(response['status'] == 1){
       				var confirmPopup = $ionicPopup.confirm({
     					title: 'Confirm your organization',
     					template: 'Is you organization name is "'+organization+ '" .'
   					});

   					confirmPopup.then(function(res) {
     					if(res) {
       						document.addEventListener("deviceready", function() {
       							window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs){
       								create_directory(fs.root,response,'customer');
								}, function(){
									console.log('ERROR')
								});
							}, false);
     					} else {
      					 console.log('You are not sure');
     					}
   					});
       			}
       			else{
       				$scope.error_msg = response['error_info'];
       			}
       			console.log(JSON.stringify(response));
        	},
        	error: function (responseData, textStatus, errorThrown) {
        		$ionicLoading.hide();
            	alert('POST failed.');
            }
  		});
	}
	$scope.$on('$ionicView.enter', function() {
	    $ionicHistory.clearHistory();
		$ionicHistory.clearCache();
     	$('#error').html("")
  	});
});

