app.controller('DiscussionDetailController',function($scope,global,$http,$rootScope,coursedetailservice){
    $scope.discussion = coursedetailservice.discussions;
    $scope.submit_reply = function(id){
        $scope.comment['d_id'] = id
        $.ajax({
            type: "POST",
            url: ajax_domain+'/lms/admin/discussion_reply/',
            data: $scope.comment,
            dataType:'json',
            timeout : 70000,
            error : function(){
                alert("check internet connection")
            },
            success : function(response){
                response['reply']['reply_msg'] = $scope.comment['reply']
                $scope.discussion['replies'].push(response['reply'])
                $scope.comment = {};
                $scope.$apply();
//               for(var i=0; i< $scope.discussions.length; i++){
//                   if($scope.discussions[i]['discussion_thread']['id'] == id){
//                        $scope.discussions[i]['replies'].push(response['reply'])
//                        console.log($scope.discussions)
//                        $scope.discussions[i]['discussion_thread']['limit_value'] = $scope.discussions[i]['replies'].length
//                        break;
//                   }
//               }
            }
        })
    }
})