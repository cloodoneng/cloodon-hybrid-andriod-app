app.controller('EnrollmentController', function($scope,$rootScope,$sce,$ionicHistory,$ionicPopup,global){
    console.log($ionicHistory.viewHistory())
    $rootScope.enrolled = 'true';
    $rootScope.remove_back_view = false;
    $scope.url = $rootScope.enrollment_url;
    $scope.trustSrc = function(src) {
        return $sce.trustAsResourceUrl(src);
    }
    $scope.e_url = $scope.trustSrc($scope.url)
    var win_height =  $(window).height();
    var ppwd;
    var ppht = win_height -60;
    $scope.embed_code = html_embed_enroll_code($scope.e_url, ppwd, ppht);
    $("#enrollment").append($scope.embed_code);
    $scope.course_view = function(){
        global.batch_id = parseInt($scope.response['batch_id']);
        global.course_title = $rootScope.title;
        $rootScope.remove_back_view = true;
        window.location = "#course-detail";
    }
    $scope.goback = function(){
        $ionicHistory.goBack()
    }
    /*
    New comment
    $scope.historyGoBack = function(){
        if($scope.response){
            if($scope.response['status'] == 'Success'){
                 $ionicHistory.goBack(-2);
            }
            else{
                $ionicHistory.goBack();
            }
        }
        else{
            $ionicHistory.goBack();
        }
    }*/
    window.addEventListener('message', receiveMessage);
    function receiveMessage(e) {
        //alert('Enrollment')
        //alert(e);
        if (e.origin !== ajax_domain)
          return;

        // Update the div element to display the message.
        console.log("Message Received: " + e.data);
        $scope.response = e.data;
        console.log(JSON.stringify($scope.response))
        $scope.enrollment_msg = $scope.response['msg']
        $scope.$emit('eventName',$scope);
        if($scope.response['status'] == 'Success'){
            $('#enrollment').remove()
            console.log($scope.enrollment_msg)
            //$("#enrollment_status").show()
            $scope.$apply();
        }
        else{
             $('#enrollment').remove();
             console.log($scope.enrollment_msg)
             $scope.$apply();
        }
    }

})