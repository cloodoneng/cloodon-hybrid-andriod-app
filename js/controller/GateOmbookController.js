app.controller('GateOmbookController', function($scope,$http,$stateParams,$rootScope,coursedetailservice,$ionicHistory,$state){
    var assessmentdata;
    params = JSON.parse($stateParams.cd)
    cid = params['cid']
    bid = params['bid']
    $scope.ombook = coursedetailservice.ombook_data['ombook_detail']
    $scope.cleared_gate = {};
    $scope.submit_popup = false;
    $scope.cid = cid
    $scope.p_id = 'quiz'
    //$scope.exit_gate = 'false';
    $scope.$emit('eventName',$scope);
    $scope.submitconfirmation = 'false';
    $http.get(ajax_domain +'/lms/content-management/ombook/start_gate/?'+'cid='+cid+'&bid'+bid).success(function(response){
        if(response['status'] == 1){
            $scope.gate_view(response["gate_ques"]);
        }
    });
    $scope.gate_view = function(gated_ombook){
        $scope.ombook_gate = gated_ombook;
        angular.forEach($scope.ombook_gate, function(value, key){

            if(value['type'] == '5'){
                    if(value['options_B'] == undefined){
                        var option_b = value['options'].slice()
                        value['options_B'] = shuffle(option_b);
                        console.log(JSON.stringify(value['options_B']))
                        angular.forEach(value['options_B'], function(op_val, op_key){
                            op_val['selected'] = false;
                            op_val['selected_op_index'] = '';
//                            if(op_val['selected'] == undefined){
//                                op_val['selected'] = false;
//                            }
//                            else{
//                            }
//                            if(op_val['selected_op_index'] == undefined){
//                               op_val['selected_op_index'] = '';
//                            }
//                            else{
//                            }

                        });
                    }
                }

            angular.forEach(value['options'],function(option,op){
                if((value['type'] == '1') || (value['type'] == '2')){
                    option['student_ans'] = false;
                }
                else{
                    option['student_ans'] = '';
                }
                //option['student_ans'] = false;
            });
        });
        $scope.question_index = 0 ;
        console.log(JSON.stringify($scope.ombook_gate))
    }
    $scope.updateSelection = function(position, options) {
        angular.forEach(options, function(value, index) {
            if (position != index)
                value.student_ans = false;
        });
    };
    $scope.unattemptedquestion = function(q_id){
        $scope.submitconfirmation = 'false';
        q = q_id-1;
        $scope.question_index = q_id-1;
    };
    $scope.next = function () {
        if ($scope.question_index == $scope.ombook_gate.length-1) {
            $scope.question_index++;
            $scope.submit();
        }
        else {
            $scope.question_index++;
        }
    };
    /*
    $scope.historyGoBack = function(){
        if($scope.submit_popup == false){
            var answer = confirm("Are you sure you want to exit from gate?");
            if(answer == true){
                $ionicHistory.goBack()
            }
            else{
            }
        }
        else if($scope.submit_popup == true){
            $ionicHistory.goBack()
        }
        else{
        }
    }
    */
    $scope.previous = function (id) {
        var ind = $scope.question_index;
        if(id == 'submitconfirmation' && $scope.submitconfirmation == 'true'){
            len = ($scope.ombook_gate).length
            $scope.submitconfirmation = 'false';
            if(ind == len){
                ind = ind -1
                $scope.question_index = ind;
            }
            else{
                $scope.question_index = ind;
            }
        }
        else{
            if ($scope.question_index == 0) {
                $scope.question_index = 0;
            }
            else {
                ind = ind -1
                $scope.question_index = ind;
            }
        }
    };
    $scope.submit = function(){
        $scope.submitconfirmation = 'true';
         //$("#timer").countdown('pause');
        var assmt_ans = [];
        var snddata = {};
        var q_index = [];
        angular.forEach($scope.ombook_gate, function(ombook, keys) {
            var que_ans = {};
            var student_answer = [];
            var bol = "false"
            angular.forEach(ombook['options'], function(value, key) {
                var opt = {};
                if((ombook['type'] == 1) || (ombook['type'] == 2)){
                    if(bol == "false"){
                        if((value['student_ans'] == true) || (value['student_ans'] != '')){
                            bol = "true";
                        }
                        else{
                            bol = "false";
                        }
                    }
                }
                else if(ombook['type'] == 4 || ombook['type'] == 8 || ombook['type'] == 5){
                    if(value['student_ans'] != ''){
                        bol = 'true'
                    }
                    else{
                        bol = 'false'
                    }
                }
                else if(ombook['type'] == 3){
                    if(bol == "false"){
                        if(value['index'] == (key+1)){
                            bol = "false";
                        }
                        else{
                            bol = "true";
                        }
                    }
                }
                if(value['student_ans'] == true){
                    opt['answer'] = "true"
                }
                else if((value['student_ans'] == false)){
                    opt['answer'] = "false"
                }
                else if((value['student_ans'] != true) && (value['student_ans'] != false)){
                    opt['answer'] = value['student_ans'];
                }
                else{
                    opt['answer'] = ''
                }
                opt['opt_id'] = value['id']
                student_answer.push(JSON.stringify(opt));

//

//                var opt = {};
//                if(bol == "false"){
//                    if(value['student_ans'] == true){
//                        bol = "true";
//                    }
//                    else{
//                        bol = "false";
//                    }
//                }
//                if(value['student_ans'] == true){
//                    opt['answer'] = "true"
//                }
//                else{
//                    opt['answer'] = "false"
//                }
//                opt['opt_id'] = value['id']
//                student_answer.push(JSON.stringify(opt));
            });
            que_ans['qid'] = ombook['id'];
            que_ans['student_answers'] = student_answer;
            que_ans['is_evaluated'] = 0;
            assmt_ans.push(que_ans);
            if(bol == "false"){
                q_index.push(keys+1);
            }
        });
        page_id = 'q'+params['cid']
        $scope.question_no = q_index;
        snddata['gate_ans'] = JSON.stringify(assmt_ans);
        snddata['cid'] = params['cid'];
        snddata['batch_id'] = params['bid']
        snddata['progress'] =JSON.stringify({"omb_id":params['oid'],"batch_id":params['bid'],"pid":page_id,"total_pages":params['total_page'],"progress_pages":params['progress']})
        assessmentdata = snddata;
        console.log(JSON.stringify(assessmentdata))
    }
    $scope.final_submission = function(){
        $.ajax({
            type: "POST",
            url: ajax_domain +'/lms/content-management/ombook/submit_gate/',
            data: assessmentdata,
            dataType:'json',
            timeout : 70000,
            error : function(){
                var ans = confirm("Oops Some Error occured.Seems to have no internet connection.Do You want to exit?")
            },
            success : function(response){
                $scope.submit_popup = true;
                if(response['status'] == 1){
                    if(response['cleared'] == true){
                        $scope.cleared_gate['message'] = "Congratulations! You have cleared the gate."
                        $scope.cleared_gate['cleared'] = 1;
                        $scope.cleared_gate['progress'] = response['progress_info']['progress']
                        $scope.cleared_gate['score'] = response['score']
                    }
                    else{
                        //$scope.cleared_gate['message'] = "Failed! You didn't Cleared this Gate try again later."
                        $scope.cleared_gate['message'] = "You have only got" + response['score'] + "answer(s) correct. Need to get atleast" + response['min_score'] +"answers correct to clear the quiz."
                        $scope.cleared_gate['cleared'] = 0;
                        $scope.cleared_gate['progress'] = response['progress_info']['progress']
                    }
                    $scope.$apply();
                    for(j=0;j<$scope.ombook['chapters'].length;j++){
                        if(cid == $scope.ombook['chapters'][j]['cid']){
                            if($scope.ombook['chapters'][j]['cleared'] == 1){
                                $scope.ombook['chapters'][j]['best_score'] = response['score']
                            }
                            else{
                                $scope.ombook['chapters'][j]['cleared'] =$scope.cleared_gate['cleared']
                                $scope.ombook['chapters'][j]['best_score'] = response['score']
                                $scope.ombook['chapters'][j]['quiz_progress'] = $scope.cleared_gate['progress']
                                if($scope.cleared_gate['cleared'] == 1){
                                    $scope.ombook['chapters'][j+1]['prev'] = true;
                                    $scope.ombook['chapters'][j+1]['prev_chap_clear'] = true;
                                }
                            }
                            $scope.$apply();
                            break;
                        }
                    }
                    coursedetailservice.ombook_data['ombook_detail'] = $scope.ombook
                    url = ajax_domain+'/apis/ombook/?callback=JSON_CALLBACK'+'&batch='+bid+'&omb='+params['oid'];
                    $http.jsonp(url).success(function(response){
                        var data = {};
                        //data['version'] = version;
                        data['ombook_id'] = params['oid'];
                        data['response'] = response;
                        onDeviceReady('ombookdetails',data,true);
                    }).error(function(){
                        console.log("error")
                    })
                }
                //$scope.exit_gate = 'true';
                //$scope.$emit('eventName',$scope);
            }
        })
    }
    $scope.back_ombook = function(cleared_gate){
        if(cleared_gate['cleared'] == 1){
            for(i=0;i<$scope.ombook['chapters'].length;i++){
                if(cid == $scope.ombook['chapters'][i]['cid']){
                    if(i < $scope.ombook['chapters'].length-1){
                        cid = $scope.ombook['chapters'][i+1]['cid']
                        if($scope.ombook['chapters'][i+1]['desc'] != ''){
                            pid= 'overview';
                        }
                        else{
                            pid= $scope.ombook['chapters'][i+1]['lpages'][0]['pid'];
                        }
                    }
                    else{
                        cid = $scope.ombook['chapters'][0]['cid']
                        if($scope.ombook['chapters'][0]['desc'] != undefined){
                            pid = undefined;
                        }
                        else{
                            pid = $scope.ombook['chapters'][0]['lpages'][0]['pid'];
                        }
                    }
                    console.log(cid)
                    console.log(pid)
                    $rootScope.cleared = true;
                    $rootScope.update_ombook_id = {'cid':cid,'pid':pid}
                    break;
                }
            }
        }
        else{
            //$rootScope.cleared = false;
            console.log(cid);
            console.log(pid)
            pid = 'quiz'
            //$rootScope.update_ombook_id = {'cid':cid,'pid':pid}
        }
        $ionicHistory.goBack()
    }
    $scope.moveItem = function(optn, fromIndex, toIndex) {
        //Move the item in the array
        $scope.ombook_gate[$scope.question_index ]['options'].splice(fromIndex, 1);
        $scope.ombook_gate[$scope.question_index ]['options'].splice(toIndex, 0, optn);
        console.log(JSON.stringify($scope.ombook_gate[$scope.question_index ]['options']))
    };
    $scope.update_match = function(match_option,ans){
        console.log("###############################")
        console.log(match_option)
        console.log(match_option['student_ans'])
        console.log(match_option['student_prev_ans'])
        console.log(ans);
        op_index = match_option['index']-1;
        console.log(op_index)
        if(ans == ''){
            ans = match_option['student_prev_ans']
        }
//        $scope.assessments[$scope.question_index]['question']['options'][op_index]['student_answer'] = ans;
        for(op_b=0;op_b<$scope.ombook_gate[$scope.question_index]['options_B'].length;op_b++){
            if($scope.ombook_gate[$scope.question_index]['options_B'][op_b]['id'] == ans){
                if($scope.ombook_gate[$scope.question_index]['options_B'][op_b]['selected'] == true){
                    $scope.ombook_gate[$scope.question_index]['options'][op_index]['student_prev_ans'] = '';
                    $scope.ombook_gate[$scope.question_index]['options_B'][op_b]['selected'] = false;
                    $scope.ombook_gate[$scope.question_index]['options_B'][op_b]['selected_op_index'] = '';
                }
                else{
                    if($scope.ombook_gate[$scope.question_index]['options'][op_index]['student_prev_ans'] == '' || $scope.ombook_gate[$scope.question_index]['options'][op_index]['student_prev_ans'] == undefined){
                        $scope.ombook_gate[$scope.question_index]['options'][op_index]['student_prev_ans'] = ans;
                        $scope.ombook_gate[$scope.question_index]['options_B'][op_b]['selected'] = true;
                        $scope.ombook_gate[$scope.question_index]['options_B'][op_b]['selected_op_index'] = op_index;
                    }
                    else{
                        reset_id = $scope.ombook_gate[$scope.question_index]['options'][op_index]['student_prev_ans']
                        for(opt_b=0;opt_b<$scope.ombook_gate[$scope.question_index]['options_B'].length;opt_b++){
                            if($scope.ombook_gate[$scope.question_index]['options_B'][opt_b]['id'] == reset_id){
                                $scope.ombook_gate[$scope.question_index]['options_B'][opt_b]['selected'] = false;
                                $scope.ombook_gate[$scope.question_index]['options_B'][opt_b]['selected_op_index'] = '';
                            }
                        }
                        $scope.ombook_gate[$scope.question_index]['options'][op_index]['student_prev_ans'] = ans;
                        $scope.ombook_gate[$scope.question_index]['options_B'][op_b]['selected'] = true;
                        $scope.ombook_gate[$scope.question_index]['options_B'][op_b]['selected_op_index'] = op_index;
                    }
                }
                console.log(JSON.stringify($scope.ombook_gate[$scope.question_index]['options_B'][op_b]))
                console.log(JSON.stringify($scope.ombook_gate[$scope.question_index]['options'][op_index]))
                break;
            }
        }
    }
    $scope.retry_now = function(){
        $state.go($state.current, {}, {reload: true});
    }

});