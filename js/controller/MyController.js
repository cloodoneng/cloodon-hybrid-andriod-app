app.controller('MyController', function($scope, $rootScope, $location, logindetailservice, $window, $ionicPlatform, $ionicHistory, $ionicLoading, $timeout) {
    $scope.thread = {};
    $scope.comment = {};
    //propagating scope value from other controller to base controller
    $scope.$on('eventName', function (event, args) {
        $scope = args;
 		console.log($scope);
 		console.log($scope.evaluated);
 	});

 	$scope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
        $ionicLoading.show({content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0});
    });

    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        $timeout(function(){
            $ionicLoading.hide();
        },2000);
    });

    var promise = logindetailservice.get_login_credential();
    promise.then(function(data) {
        console.log(data);
        var customer_data = JSON.parse(data['customer_data']);
        if(customer_data){
            logo = customer_data['logo'];
            //ajax_domain = customer_data['domain_name'];
            ajax_domain = 'http://192.168.254.46';
        }
        if(data['users_credential'] != '{}' || data['users_credential'] == 'undefined'){
            var users_credential = JSON.parse(data['users_credential']);
            login(users_credential);
        }
        else{
            window.location = "#login";
        }
    }, function(reason) {
        console.log('Failed: ' + reason);
        window.location = "#organization";
    }, function(update) {
        console.log('Got notification: ' + update);
    });

    function login(user_credential){

        if(user_credential == undefined){
            window.location = '#login';
        }
        else{
            var user_data = {}
            user_data['username'] = user_credential['username'];
            user_data['password'] = user_credential['password'];
            $rootScope.user = user_credential;
  		    logindetailservice.send_user_credentail(user_credential, false);
        }
    }
    $scope.BackViewCall = function(){
        var history = $ionicHistory.currentView();
        console.log(JSON.stringify(history));
        if(history['url'] == '/previewassessment' && $scope.evaluated == "false"){
            var answer = confirm("Do you want to save and resume?");
            if(answer == true){
                $scope.save_resume(false);
                $ionicHistory.goBack()
            }
            else{
            }
        }
        else if(history['stateName'] == "ombook_start_gate" && $scope.submit_popup == false){
            var answer = confirm("Are you sure you want to exit from gate?");
            if(answer == true){
                $ionicHistory.goBack()
            }
            else{
            }
        }
        else if(history['url'] == "/course-detail" && $rootScope.remove_back_view == true){
            $scope.history = $ionicHistory.viewHistory()
            console.log($scope.history['histories']['root']['stack'].length)
            $scope.back_value = $scope.history['histories']['root']['stack'].length -1;
            $ionicHistory.goBack(-$scope.back_value);
        }
        else if(history['url'] == "/enroll"){
            if($scope.response){
                if($scope.response['status'] == 'Success'){
                    $ionicHistory.goBack(-2);
                }
                else{
                    $ionicHistory.goBack();
                }
            }
            else{
                $ionicHistory.goBack();
            }
        }
        else{
            $ionicHistory.goBack();
        }
    }
    $scope.AppBack = function(){
        $scope.BackViewCall();
    }
    $ionicPlatform.registerBackButtonAction(function () {
        $scope.BackViewCall();
//        var history = $ionicHistory.currentView();
//        console.log(JSON.stringify(history));
//        if(history['url'] == '/previewassessment' && $scope.evaluated == "false"){
//            var answer = confirm("Do you want to save and resume?");
//            if(answer == true){
//                $scope.save_resume(false);
//                $ionicHistory.goBack()
//            }
//            else{
//            }
//        }
//        else if(history['stateName'] == "ombook_start_gate" && $scope.submit_popup == false){
//            var answer = confirm("Are you sure you want to exit from gate?");
//            if(answer == true){
//                $ionicHistory.goBack()
//            }
//            else{
//            }
//        }
//        else if(history['url'] == "/course-detail" && $rootScope.remove_back_view == true){
//            $scope.history = $ionicHistory.viewHistory()
//            console.log($scope.history['histories']['root']['stack'].length)
//            $scope.back_value = $scope.history['histories']['root']['stack'].length -1;
//            $ionicHistory.goBack(-$scope.back_value);
//        }
//        else if(history['url'] == "/enroll"){
//            if($scope.response){
//                if($scope.response['status'] == 'Success'){
//                    $ionicHistory.goBack(-2);
//                }
//                else{
//                    $ionicHistory.goBack();
//                }
//            }
//            else{
//                $ionicHistory.goBack();
//            }
//        }
//        else{
//            $ionicHistory.goBack();
//        }
    }, 100);
});