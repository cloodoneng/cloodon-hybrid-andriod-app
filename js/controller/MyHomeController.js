app.controller('MyHomeController',function($scope, global, $http, $rootScope, CommonService, CourseCatalogDetailService, $ionicSideMenuDelegate, $ionicHistory){
   	ionic.Platform.ready(function(){
    });
    //$scope.browse_course = catalog;
    var promise = CourseCatalogDetailService.get_users_course();
    promise.then(function(data) {
        $scope.viewscopes(data)
//        if(data['status'] == 1){
//
//        }
    }, function(reason) {
        console.log('Failed: ' + reason);
    }, function(update) {
        console.log('Got notification: ' + update);
    });

//    document.addEventListener("deviceready", function() {
//       					window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs){
//       						create_directory(fs.root,{},'test');
//						}, function(){
//							console.log('ERROR')
//						});
//					}, false);
//    var promise = CourseCatalogDetailService.get_course_catalog();
//    promise.then(function(data) {
//        console.log(JSON.stringify(data))
//        if(data['status'] == 1){
//            $scope.browse_course = data;
//        }
//    }, function(reason) {
//        console.log('Failed: ' + reason);
//    }, function(update) {
//        console.log('Got notification: ' + update);
//    });
    $rootScope.enrolled = 'false';
    $scope.user = $rootScope.user
    $scope.viewscopes = function(response){
        //$scope.mycourses = response['courses'];
        $scope.enrolledcourse = response['courses'];
        console.log(response)
        console.log($scope.enrolledcourse)
        $scope.users_classes = response['myclasses'];
        //console.log($scope.users_classes);
        global.course_list = response;
//        angular.forEach($scope.mycourses, function(value, key) {
//            if(value['state'] == 'Live' || value['state'] == 'Open' || value['state'] == 'Draft' || value['state'] == 'Closed'){
//                $scope.enrolledcourse.push(value);
//            }
//        });
    }
//    if(usercourse['status'] == 1){
//   	    $scope.viewscopes(usercourse['courses'])
//   	}
    $scope.batchdetail = function(batch_id,course_title){
        global.course_title = course_title;
        global.batch_id = batch_id;
        window.location = "#course-detail";
    }
    $scope.browse_caourse_detail = function(course){
        CommonService.get_catalog_course(course);
        window.location = "#course"
    }
    $scope.get_course = function(course_cat){
        CommonService.get_catalog(course_cat);
        //CourseCatalogDetailService.catalog_course_list = course_cat;
        window.location="#catalog-course-list/"+course_cat['name'];
    }
    $scope.toggleLeft = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };
    $scope.logout = function(){
        var ans = confirm("Are u sure you want to logout..You will loss all your data")
        if(ans == true){
            url=ajax_domain+'/apis/logout/?callback=JSON_CALLBACK&session_id='+ $rootScope.session_id;
            $http.jsonp(url).success(function(response){
                if(response['status'] == 1){
                    $ionicHistory.clearCache();
                    $ionicHistory.clearHistory();
                    var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
                    db.transaction(function(tx){
                        tx.executeSql('DROP TABLE IF EXISTS assessmentlists');
                        tx.executeSql('DROP TABLE IF EXISTS updateprogress');
                        tx.executeSql('DROP TABLE IF EXISTS coursedetail');
                        tx.executeSql('DROP TABLE IF EXISTS ombookdetails');
                        tx.executeSql('DROP TABLE IF EXISTS assessmentdetails');
                        tx.executeSql('DROP TABLE IF EXISTS usercredential');
                        tx.executeSql('DROP TABLE IF EXISTS coursecatalogdetail');
                        tx.executeSql('DROP TABLE IF EXISTS courselist');
                    }, function (err) {
                        console.log("Error processing SQL: "+err.code);
                    }, function() {
                        console.log("success");
                    });
                    //db.changeVersion("1.1", "1.0");
                    //         window.deleteDatabase({name: "Database"}, function(){
                    //            alert("Successfully deleted database");
                    //        }, function(){
                    //            alert("Error while delete database");
                    //        });
                    window.location = "#login"
                }
                else{
                    alert("Some error occured")
                }
           }).error(function(){
                alert('no internet connection')
           })
        }
        else{
        }
    }

    $scope.$on("$ionicView.enter", function(event, data){
        if($rootScope.enrolled == 'true'){
            var promise = CourseCatalogDetailService.get_users_course();
            promise.then(function(data) {
                if(data['status'] == 1){
                    $scope.viewscopes(data['courses'])
                }
            }, function(reason) {
                console.log('Failed: ' + reason);
            }, function(update) {
                console.log('Got notification: ' + update);
            });
            $rootScope.enrolled = 'false';
        }
    });
    $scope.joingotomeeting = function(e,meeting_id){
        e.stopPropagation();
        url = 'https://global.gotomeeting.com/join/' +  meeting_id;
        window.open(url, '_system');
        //popup = window.open(url, 'popup_window');
        //var ref = $window.open(url, '_blank', 'location=yes,scrollbars=yes,status=yes,clearsessioncache=yes,clearcache=yes');

//        var params = [
//            'height='+screen.height,
//            'width='+screen.width
//            //'fullscreen=yes' // only works in IE, but here for completeness
//        ].join(',');
//        var popup = window.open(url, 'popup_window', params);
//        if(typeof popup == 'undefined'){
//            popup = window.open(url, 'popup_window');
//        }
        //popup.moveTo(0,0);
    }
});