app.controller('OmBookController',function($scope,global,$http,$rootScope,coursedetailservice,$q,$interval,$stateParams,$compile,ombookservice,$window,$ionicHistory, $ionicPopover,$ionicLoading,$timeout,$ionicModal,$location,$anchorScroll,$ionicNavBarDelegate){
    $scope.omb_title = coursedetailservice.ombook_data['ombook_detail']['title']
    $scope.title = global.course_title;
    //$window.progress_store = [];
    progress_store = [];
    var currPage;
    var next_chapter;
    var next_page;
    var ombook_html;
    var youtubedic = {};
    $scope.batch_id = global.batch_id;
    $scope.pno = 0
    var total_page = 0;
    $scope.total_page = 0
    var progress = 0;
    var limit = 60;
    progrss_list = [];
    $scope.om_update = ombookservice['ombook_details'];
    document.addEventListener("deviceready", function() {
        console.log(Media);
        platform_name = device.platform;
        console.log(platform_name);
//        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs){
//            fs.root.getFile('practice_recording.mp3', {create: true, exclusive: false}, function(fileEntry) {
//  			    src = fileEntry.toInternalURL();
//  			},function(){console.log('error')}
//  		);
//		}, function(){
//		    console.log('ERROR')
//		});
	}, false);
    $scope.ombook = coursedetailservice.ombook_data['ombook_detail']
        for(i=0;i<$scope.ombook['chapters'].length;i++){
            if($scope.ombook['chapters'][i]['desc'] != ''){
                total_page = total_page + 1
            }
            if($stateParams.cid == $scope.ombook['chapters'][i]['cid']){
                $scope.chapter_no = i;
                if($stateParams.pid == 'overview'){
                    $scope.page_no = undefined;
                }
                else if($stateParams.pid == 'quiz'){
                    $scope.page_no = $scope.ombook['chapters'][i]['lpages'].length;
                }
                else{
                    for(j=0;j<$scope.ombook['chapters'][i]['lpages'].length;j++){
                        if($scope.ombook['chapters'][i]['lpages'][j]['pid'] == $stateParams.pid){
                            $scope.page_no = j;
                            currPage = $('#chap-'+$stateParams.cid+'-page-'+$stateParams.pid)
                        }
                    }
                }
            }
            if($scope.ombook['chapters'][i]['gate_questions'] != false){
                total_page = total_page +1;
            }
            chap_key = "c"+$scope.ombook['chapters'][i]['cid'];
            quiz_key = "q"+$scope.ombook['chapters'][i]['cid'];
            for(j=0;j<$scope.ombook['chapters'][i]['lpages'].length;j++){
                total_page =total_page + 1;
                chap_page = $scope.ombook['chapters'][i]['lpages'][j]['pid']
                if($scope.ombook['progress'].hasOwnProperty(chap_page)){
                    if($scope.ombook['progress'][chap_page] == 1){
                        progress =progress + 1;
                    }
                }
            }
            if($scope.ombook['progress'].hasOwnProperty(chap_key)){
                if($scope.ombook['progress'][chap_key] == 1){
                    progress =progress + 1;
                }
            }
            if($scope.ombook['progress'].hasOwnProperty(quiz_key)){
                if($scope.ombook['progress'][quiz_key] == 1){
                    progress =progress + 1;
                }
            }
        }

    //$scope.start_ombook();
    $scope.$emit('eventName',$scope);
    //New code for progress updation
    var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
    db.transaction(function(tx) {
        tx.executeSql('SELECT * FROM updateprogress  WHERE id = 1',[], function(tx, results) {
            var len = results.rows.length;
            console.log("updateprogress table: " + len + " rows found.");
            for (var i=0; i<len; i++){
                console.log("Row = " + i + " ID = " + results.rows.item(i).id + " Data =  " + results.rows.item(i).progress_data);
                d = results.rows.item(i).progress_data;
                ombookservice.update_ombook_progress(d)
            }
            tx.executeSql('DROP TABLE IF EXISTS updateprogress');
        }, function(err) {
            console.log(err)
            console.log("Error processing SQL error executesql: "+err.code+err.message);
        });
    }, function (err) {
        console.log("Error processing SQLtransectiondb: "+err.code+err.message);
    });
    //end of the code
    $ionicPopover.fromTemplateUrl('templates/popover.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.popover = popover;
    })
    $scope.next = function(){
//        $ionicLoading.show({content: 'Loading',
//            animation: 'fade-in',
//            showBackdrop: true,
//            maxWidth: 200,
//            showDelay: 0
//        });
        $("#anotation_button").hide();
        var data = $('.active-page').data()
        chapter = data.chapter
        cid = data.chapter.cid
        page_no =  data.pageKey != 'undefined' ? data.pageKey : undefined;
        pid = data.pageId != 'undefined' ? data.pageId : undefined;
        progress_g = data.pageProgress
        $scope.VideoPause(cid,pid,page_no)
        $scope.update_progress(cid,pid,progress_g)
        if(page_no != chapter.lpages.length && page_no != undefined && chapter.lpages[page_no+1]){
            if(chapter.lpages[page_no+1].ptype == "4" || chapter.lpages[page_no+1].ptype == "9"){
                $('.omb-nav').removeClass('hide')
            } else {
                $('.omb-nav').addClass('hide')
            }
        }
        if($scope.page_no >= 0){
            if($scope.page_no < chapter['lpages'].length-1){
                $scope.page_no = $scope.page_no + 1;
                next_page = $scope.page_no;
            }
            else if($scope.page_no == chapter['lpages'].length-1 && chapter['gate_questions']){
                $scope.gate_question = true;
                $scope.page_no = $scope.page_no + 1;
                next_page = $scope.page_no
            }
            else if($scope.page_no == chapter['lpages'].length || (($scope.page_no == chapter['lpages'].length -1) && chapter['gate_questions'] == false)){
                if($scope.chapter_no < $scope.ombook['chapters'].length-1){
                    if($scope.page_no == chapter['lpages'].length){
                        if(chapter['gate_questions']>0 && (chapter['cleared'] == 1)){
                            $scope.chapter_no = $scope.chapter_no + 1;
                            $scope.gated = false;
                        }
                        else if(chapter['gate_questions']>0){
                            alert("YOU NEED TO CLEAR THIS ASSESSMENT");
                            $scope.gated = true;
                        }
                    }
                    else{
                        $scope.chapter_no = $scope.chapter_no + 1;
                        $scope.gated = false;
                    }
                }
                else{

                }
                $scope.gate_question = false;
                if($scope.gated == false){
                    for(i=0;i<$scope.ombook['chapters'].length; i++){
                        if($scope.ombook['chapters'][i]['cid'] == chapter['cid'] && i<$scope.ombook['chapters'].length-1){
                            if($scope.ombook['chapters'][i+1]['desc'] != ''){
                                $scope.page_no = undefined;
                                next_page = $scope.page_no
                                next_chapter = $scope.ombook['chapters'][i+1]['cid']
                                break;
                            }
                            else{
                                $scope.page_no =0;
                                next_page = $scope.page_no;
                                next_chapter = $scope.ombook['chapters'][i+1]['cid']
                                break;
                            }
                        }
                    }
                }
            }
        }
        else{
            $scope.page_no = 0;
            next_chapter = chapter['cid']
            next_page = $scope.page_no
        }
        if((($scope.gated == false) || ($scope.gated == undefined)) && (data.pageNo != total_page)){
            c_page_no = data.pageNo +1;
        }
        if((next_page != undefined) && (next_page != chapter['lpages'].length)){
            if($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['atn'] != ''){
                $("#anotation_button").show();
            }
        }
        $scope.Page_creation();
        $("#timer").countdown('destroy')
        if($scope.page_no == undefined){
            $("#timer").countdown({until:limit,format:'S'})
            $scope.timer = true;
        }
        else if(($scope.page_no != undefined && ($scope.page_no != ($scope.ombook['chapters'][$scope.chapter_no]['lpages']).length)) && ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] == 4 || $scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] == 9) ){
            $scope.timer = false;
        }
        else if(($scope.page_no != ($scope.ombook['chapters'][$scope.chapter_no]['lpages']).length) && ($scope.page_no == undefined || ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] != 4) || ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] != 9))){
            $("#timer").countdown({until:limit,format:'S'})
            $scope.timer = true;
        }
        else{
        }
//        $timeout(function(){
//            $ionicLoading.hide();
//        },2000);
    }
    $scope.prev = function(){
//        $ionicLoading.show({content: 'Loading',
//            animation: 'fade-in',
//            showBackdrop: true,
//            maxWidth: 200,
//            showDelay: 0
//        });
        $("#anotation_button").hide();
        var data = $('.active-page').data()
        chapter = data.chapter
        cid = data.chapter.cid
        page_no =  data.pageKey != 'undefined' ? data.pageKey : undefined;
        pid = data.pageId != 'undefined' ? data.pageId : undefined;
        progress_g = data.pageProgress
        if(data.pageNo != 1){
            c_page_no = data.pageNo -1;
        }
        if(page_no != 0 && page_no != undefined){
            if(chapter.lpages[page_no-1].ptype == "4" || chapter.lpages[page_no-1].ptype == "9"){
                $('.omb-nav').removeClass('hide')
            } else {
                $('.omb-nav').addClass('hide')
            }
        }
        $scope.VideoPause(cid,pid,page_no)
        $scope.update_progress(cid,pid,progress_g)
        $scope.gated = false;
        if($scope.page_no == 0){
            if(chapter['desc'] != ''){
                $scope.page_no = undefined;
                next_page = $scope.page_no
                next_chapter = chapter['cid']
            }
            else{
                if($scope.chapter_no > 0){
                    $scope.chapter_no = $scope.chapter_no - 1;
                    for(i=0;i<$scope.ombook['chapters'].length;i++){
                        if($scope.ombook['chapters'][i]['cid'] == chapter['cid']){
                            if($scope.ombook['chapters'][i-1]['gate_questions'] > 0){
                                $scope.page_no = $scope.ombook['chapters'][i-1]['lpages'].length
                                next_page = $scope.page_no
                                next_chapter = $scope.ombook['chapters'][i-1]['cid']
                            }
                            else{
                                $scope.page_no = $scope.ombook['chapters'][i-1]['lpages'].length - 1;
                                next_page = $scope.page_no
                                next_chapter = $scope.ombook['chapters'][i-1]['cid']
                            }
                        }
                    }
                }
                else{

                }
            }
        }
        else if($scope.page_no == undefined){
            if($scope.chapter_no > 0){
                $scope.chapter_no = $scope.chapter_no - 1;
                for(i=0;i<$scope.ombook['chapters'].length;i++){
                    if($scope.ombook['chapters'][i]['cid'] == chapter['cid']){
                        if($scope.ombook['chapters'][i-1]['gate_questions'] > 0){
                            $scope.page_no = $scope.ombook['chapters'][i-1]['lpages'].length
                            next_page = $scope.page_no
                        }
                        else{
                            $scope.page_no = $scope.ombook['chapters'][i-1]['lpages'].length - 1;
                            next_page = $scope.page_no
                        }
                        next_chapter = $scope.ombook['chapters'][i-1]['cid']
                    }
                }
            }
            else{

            }
        }
        else{
            $scope.page_no = $scope.page_no -1;
            next_page = $scope.page_no
            next_chapter = chapter['cid']
        }
        if((next_page != undefined) && (next_page != $scope.ombook['chapters'][$scope.chapter_no]['lpages'].length)){
            if($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['atn'] != ''){
                $("#anotation_button").show();
            }
        }
        $scope.Page_creation()
        $("#timer").countdown('destroy')
        if($scope.page_no == undefined){
            $("#timer").countdown({until:limit,format:'S'})
            $scope.timer = true;
        }
        else if(($scope.page_no != undefined && ($scope.page_no != ($scope.ombook['chapters'][$scope.chapter_no]['lpages']).length)) && ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] == 4 || $scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] == 9) ){
            $scope.timer = false;
        }
        else if(($scope.page_no != ($scope.ombook['chapters'][$scope.chapter_no]['lpages']).length) && ($scope.page_no == undefined || ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] != 4) || ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] != 9))){
            $("#timer").countdown({until:limit,format:'S'})
            $scope.timer = true;
        }
        else{
        }
//        $timeout(function(){
//            $ionicLoading.hide();
//        },2000);
    }
    $scope.VideoPause = function(cid,pid,page_no){
        if(cid && pid){
            currPage = $('#chap-'+cid+'-page-'+pid)
        }
        if(currPage){
            var currvideo = currPage.find('video');
            var curraudio = currPage.find('audio')
            var youtubevideo = '#content-'+cid+'-'+page_no
            var ytplrid = $('#chap-'+cid+'-page-'+pid).find('.ytplr').attr('id')
            var curobj = '#content-'+cid+'-'+page_no
            var ooid = $(curobj+">.ooplr").attr("ooid");
            if(($window.ooplrlist).hasOwnProperty(ooid)){
                ($window.ooplrlist)[ooid].pause();
            }
            if(currvideo.length >0){
                currvideo[0].pause();
            }
            if(curraudio.length>0){
                curraudio[0].pause();
            }
            if(youtubedic.hasOwnProperty(youtubevideo)){
                if(youtubedic[youtubevideo].getPlayerState() == 1){
                     youtubedic[youtubevideo].pauseVideo();
                }
            }
            if(currPage.find(".vimeo_video").length > 0){
                $("#content-"+cid+"-"+page_no).empty();
            }
        }
    }
    $scope.question_embed = function(qjson){
        var win_height =  $(window).height();
        var page_width =  $(window).width();
        if(win_height <  page_width){
            var temp = win_height;
            win_height = page_width;
            page_width = win_height;
        }
        else{
        }
        var ppwd = page_width;
        var ppht = win_height -60;
        var dwd = ppwd;
        if(qjson['r_cat'] == '4'){
            $scope.ques_embed = video_embed_code(qjson['r_url'],qjson['r_local'],ppwd,ppht)
        }
        else if(qjson['r_cat'] == '9'){
             $scope.ques_embed = html5_audio_embed_code(qjson['r_url'], ppwd);
        }
        else if(qjson['r_cat'] == '6'){
            if(isLocal){
                $scope.ques_embed = image_embed_code(qjson['r_url'], dwd, ppht);
            }
            else{
                if(checkimage(url)){
                    $scope.ques_embed = image_embed_code(qjson['r_url'], dwd, ppht);
                }
                else{
                    $scope.ques_embed = html_embed_code(qjson['r_url'], ppwd, ppht);
                }
            }
        }
        return $scope.ques_embed
    }
    $scope.embed = function(page){
        var win_height =  $(window).height();
        var page_width =  $(window).width();
        if(win_height <  page_width){
            var temp = win_height;
            win_height = page_width;
            page_width = win_height;
        }
        else{

        }
        var ppwd = page_width;
        var ppht = win_height -60;
        $scope.embed_code = undefined;
        if(page.ttype == 2){
            if(page['qjson'] != ''){

            }
        }
        else{
            if(page.ptype == 5){
                $scope.embed_code = unescape(page.html);
                return $scope.embed_code;
            }
            else{
                $scope.embed_code = get_embed_code(page,ppwd,ppht);
                return $scope.embed_code;
            }
        }
        console.log($scope.embed_code);
    }
    $scope.start_ombook = function(cache){
         $("#anotation_button").hide();
        next_page = $scope.page_no;
        if(cache == true){
            next_chapter = $scope.update_cid;
            var data = $('.active-page').data()
            c_page_no = data.pageNo +1;
        }
        else{
            next_chapter = $stateParams.cid;
            var data = $('.active-page').data()
            c_page_no = data.pageNo;
        }
        $scope.Page_creation();
        if($scope.page_no == undefined){
            $("#timer").countdown({until:limit,format:'S'})
            $scope.timer = true;
        }
        else if(($scope.page_no != undefined && ($scope.page_no != ($scope.ombook['chapters'][$scope.chapter_no]['lpages']).length)) && ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] == 4 || $scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] == 9) ){
            $('.omb-nav').removeClass('hide')
            $scope.timer = false;
        }
        else if(($scope.page_no != ($scope.ombook['chapters'][$scope.chapter_no]['lpages']).length) && ($scope.page_no == undefined || ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] != 4) || ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] != 9))){
            $("#timer").countdown({until:limit,format:'S'})
            $scope.timer = true;
        }
        else{
        }
        if((next_page != undefined) && (next_page != $scope.ombook['chapters'][$scope.chapter_no]['lpages'].length)){
            if($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['atn'] != ''){
                $("#anotation_button").show();
            }
        }
    }
    $scope.$on('onRepeatLast', function(scope, element, attrs){
        $scope.start_ombook();
//        $("#anotation_button").hide();
//        next_page = $scope.page_no;
//        next_chapter = $stateParams.cid;
//        var data = $('.active-page').data()
//        c_page_no = data.pageNo;
//        $scope.Page_creation();
//        if($scope.page_no == undefined){
//            $("#timer").countdown({until:limit,format:'S'})
//            $scope.timer = true;
//        }
//        else if(($scope.page_no != undefined && ($scope.page_no != ($scope.ombook['chapters'][$scope.chapter_no]['lpages']).length)) && ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] == 4 || $scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] == 9) ){
//            $('.omb-nav').removeClass('hide')
//            $scope.timer = false;
//        }
//        else if(($scope.page_no != ($scope.ombook['chapters'][$scope.chapter_no]['lpages']).length) && ($scope.page_no == undefined || ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] != 4) || ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] != 9))){
//            $("#timer").countdown({until:limit,format:'S'})
//            $scope.timer = true;
//        }
//        else{
//        }
//        if((next_page != undefined) && (next_page != $scope.ombook['chapters'][$scope.chapter_no]['lpages'].length)){
//            if($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['atn'] != ''){
//                $("#anotation_button").show();
//            }
//        }
    });
    $scope.check_answer = function(cid,pid){
        for(i=0;i<$scope.ombook['chapters'].length;i++){
            if($scope.ombook['chapters'][i]['cid'] == cid){
                for(p=0;p<$scope.ombook['chapters'][i]['lpages'].length;p++){
                    if($scope.ombook['chapters'][i]['lpages'][p]['pid'] == pid){
                        for(op=0;op<$scope.ombook['chapters'][i]['lpages'][p]['qjson']['options'].length;op++){
                            if(($scope.ombook['chapters'][i]['lpages'][p]['qjson']['options'][op]['student_ans'] == true) && ($scope.ombook['chapters'][i]['lpages'][p]['qjson']['options'][op]['answer'] == 'true')){
                                $scope.ombook['chapters'][i]['lpages'][p]['attempt'] = 'Correct';
                            }
                            else if(($scope.ombook['chapters'][i]['lpages'][p]['qjson']['options'][op]['student_ans'] == true) && ($scope.ombook['chapters'][i]['lpages'][p]['qjson']['options'][op]['answer'] == 'false')){
                                $scope.ombook['chapters'][i]['lpages'][p]['attempt'] = 'InCorrect';
                                break;
                            }
                            else if(($scope.ombook['chapters'][i]['lpages'][p]['qjson']['options'][op]['student_ans'] == false) && ($scope.ombook['chapters'][i]['lpages'][p]['qjson']['options'][op]['answer'] == 'true')){
                                $scope.ombook['chapters'][i]['lpages'][p]['attempt'] = 'InCorrect';
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    $scope.create_page = function(next_page){
        cp = $scope.chapter_no
        if(next_page == undefined){
            ombook_html = $scope.ombook['chapters'][cp]['desc']
        }
        else if(next_page == $scope.ombook['chapters'][cp]['lpages'].length){
             ombook_html = '<div class="app-container"><div ng-if="('+$scope.ombook['chapters'][cp]['cleared'] +' == 0) || ('+ $scope.ombook['chapters'][cp]['cleared'] +'== undefined) ">'+
                            '<div class="row">You need to clear this quiz to proceed ahead </div></div>'+
                            '<div ng-if="('+$scope.ombook['chapters'][cp]['cleared'] +' == 1)">'+
                            '<div class="row">You have cleared this quiz</div><div ng-if="'+$scope.ombook['chapters'][cp]['best_score'] + '!= null">,your best score is:'+$scope.ombook['chapters'][cp]['best_score']+'</div></div>'+
                            '<h5>Instructions</h5>'+
                            '<li >Total questions:' + $scope.ombook['chapters'][cp]['gate_questions']+ '</li>'+
                            '<li>Minimum correct answers to clear the quiz:'+ $scope.ombook['chapters'][cp]['minimum_correct_question']+'</li>'+
                            '<li>You can retry this quiz to improve your score ( Best attempt score is considered )</li>'+
                            '<li>Click on "Submit" when you are done</li>'+
                            '<li>You cannot save and resume later</li>'+
                            '<div class="row"><button class="button button-positive button-clear" ng-click="prev()">'+'Try Latter</button>'+
                            '<button class="button button-positive" ng-click="start_gate('+$scope.ombook['chapters'][cp]['cid']+','+$scope.batch_id+')">'+'Start</button>'+
                            '</div></div>';
                            //'<div class="row"><button class="button button-positive" ng-click="start_gate('+$scope.ombook['chapters'][cp]['cid']+','+$scope.batch_id+')">'+'Start</button></div>'
        }
        else{
            var page_con = $scope.ombook['chapters'][cp]['lpages'];
            if(page_con[next_page]['ttype'] == '2' && page_con[next_page]['qjson'] != ''){
                r_cat = page_con[next_page]['qjson']['r_cat']
                if(r_cat == 4 || r_cat == 9 || r_cat == 6){
                    $scope.embed_question = $scope.question_embed(page_con[next_page]['qjson'])
                }
                var option = ''
                for(op=0;op<page_con[next_page]['qjson']['options'].length;op++){
                    var s_ans = page_con[next_page]['qjson']['options'][op];
                    var z = "ombook['chapters']["+cp+"]['lpages']["+next_page+"]"
                    if(page_con[next_page]['qjson']['type'] == '1'){
                        option = option + '<ion-list class="omb-ques-option"><ion-checkbox ng-model="' + z+"['qjson']['options']["+op+"]['student_ans']" + '">' + page_con[next_page]['qjson']['options'][op].text + '</ion-checkbox></ion-list>';
                    }
                    else if(page_con[next_page]['qjson']['type'] == '2'){
                        option = option + '<ion-list class="omb-ques-option"><ion-checkbox ng-model="' + z+"['qjson']['options']["+op+"]['student_ans']" + '" ng-click="updateSelection('+op+','+$scope.ombook['chapters'][cp]['cid']+','+page_con[next_page]['pid']+')" >' + page_con[next_page]['qjson']['options'][op].text + '</ion-checkbox></ion-list>';
                    }
                }
                 if($scope.embed_question){
                    ombook_html ='<div class="omb-ques"><p>'+ page_con[next_page]['qjson']['question'] + '</p>' +
                        $scope.embed_question+option +
                        '<div ng-class="{'+"'show':"+z+"['attempt']" + '== '+"'Correct'" +",'hide':("+z+"['attempt'] == undefined ||"+z+"['attempt']" + '== '+"'InCorrect') == true"+'}">' + '<p>Correct</p> </div>' + '<div ng-class="{'+"'show':"+z+"['attempt']" + '== '+"'InCorrect'" +",'hide':("+z+"['attempt'] == undefined ||" +z+"['attempt']" + '== '+"'Correct') == true"+'}">' + '<p>InCorrect</p> </div>'  +
                        '<button type="button" ng-click="check_answer(' +$scope.ombook["chapters"][cp]["cid"] +',' + page_con[next_page]["pid"] +')" class="btn btn-default">Check Answer</button>' + "</p>";
                 }
                 else{
                    ombook_html = '<div class="omb-ques"><p>' + page_con[next_page]['qjson']['question'] + '</p>' +
                        option +
                        '<div ng-class="{'+"'show':"+z+"['attempt']" + '== '+"'Correct'" +",'hide':("+z+"['attempt'] == undefined ||"+z+"['attempt']" + '== '+"'InCorrect') == true"+'}">' + '<p>Correct</p> </div>' + '<div ng-class="{'+"'show':"+z+"['attempt']" + '== '+"'InCorrect'" +",'hide':("+z+"['attempt'] == undefined ||" +z+"['attempt']" + '== '+"'Correct') == true"+'}">' + '<p>InCorrect</p> </div>'  +
                        '<button type="button" ng-click="check_answer(' +$scope.ombook["chapters"][cp]["cid"] +',' + page_con[next_page]["pid"] +')" class="button button-block button-assertive">Check Answer</button>' + '</div>';
                 }
            }
            else{
                ombook_html = $scope.embed(page_con[next_page])
            }
        }
    }
    $scope.updateSelection = function(position,chapid,page_id) {
        for(c=0;c<$scope.ombook['chapters'].length;cp++){
            if($scope.ombook['chapters'][c]['cid'] == chapid){
                for(p=0;p<$scope.ombook['chapters'][c]['lpages'].length;p++){
                    if($scope.ombook['chapters'][c]['lpages'][p]['pid'] == page_id){
                        for(o=0;o<$scope.ombook['chapters'][c]['lpages'][p]['qjson']['options'].length;o++){
                            if(o != position){
                                $scope.ombook['chapters'][c]['lpages'][p]['qjson']['options'][o]['student_ans'] = false;
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
    };
    $scope.slide_play = function(){
        var youtubevideo = '#content-'+next_chapter+'-'+next_page;
        if(youtubedic.hasOwnProperty(youtubevideo)){
            if(youtubedic[youtubevideo].getPlayerState() == 1){
                youtubedic[youtubevideo].pauseVideo();
            }
            else if(youtubedic[youtubevideo].getPlayerState() == 2 || youtubedic[youtubevideo].getPlayerState() == 5){
                youtubedic[youtubevideo].playVideo();
            }
        }
    }
    $scope.start_gate = function(cid,bid){
        cd = {'cid':cid,'bid':bid,'oid':$scope.ombook['id'],'total_page':total_page,'progress':progress}
        window.location='#ombook_start_gate/'+JSON.stringify(cd);
    }
    $scope.Page_creation = function(){
        //$scope.total_page_of_ombook = $('.omb-page').length
        $scope.total_page_of_ombook = total_page
        //            $scope.current_page_no = $('.omb-page').index($('.active-page')) + 1;
        $scope.current_page_no = c_page_no
        $('.ombook-toc a').removeClass('current')
        $('#'+$scope.current_page_no).addClass('current')
        if(next_chapter){
            if($('#content-'+ next_chapter +'-' +next_page).length >0){
                if($('#content-'+ next_chapter +'-' +next_page)[0].hasChildNodes()){

                }
                else{
                    $ionicLoading.show({content: 'Loading',
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showDelay: 0
                    });
                    $scope.create_page(next_page)
                    console.log('###################################')
                    console.log(ombook_html)
                    $('#content-'+ next_chapter +'-' +next_page).append($compile(ombook_html)($scope));
                    if($('#content-'+next_chapter +'-' +next_page)[0].hasChildNodes()){
                        if($('#content-'+next_chapter +'-'+next_page).find('.ooplr').length > 0){
                            var page_height =  $(window).height() - 60;
                            init_oo_players(page_height);
                        }
                    }
                    var pageid = '#content-'+next_chapter+'-'+next_page
                    var yid=$('#content-'+ next_chapter +'-' +next_page).find('.ytplr').attr('id')
                    if(yid){
                        if(youtubedic.hasOwnProperty(pageid)){
                        }
                        else{
                            player = new YT.Player(yid, {
                                events: {
                                }
                            });
                            youtubedic[pageid] = player;
                        }
                    }
                    $timeout(function(){
                        $ionicLoading.hide();
                    },2000);
                }
            }
        }
    }
    $scope.update_progress = function(cid,pid,r_progress){
        var curr_progress;
        if($scope.timer ==false){
            if($window.state){
                curr_progress = $window.state
                $window.state = undefined;
            }
            else{
                var curobj = '#content-'+cid+'-'+$scope.page_no
                var ooid = $(curobj+">.ooplr").attr("ooid");
                var currvideo = $(curobj).find('video');
                var curraudio = $(curobj).find('audio');
                //var youtubevideo = '#content-'+cid+'-'+page_no
                var ytplrid = $('#chap-'+cid+'-page-'+pid).find('.ytplr')
                if(ytplrid.length >0){
                    if(youtubedic.hasOwnProperty(curobj)){
                        if(youtubedic[curobj].getPlayerState() == 0){
                            curr_progress =1;
                        }
                        else if((youtubedic[curobj].getPlayerState() == 2) || (youtubedic[curobj].getPlayerState() == 1)){
                            curr_progress = 2;
                        }
                    }
                }
                else if(ooid){
                    if(curr_progress){
                    }
                    else{
                        if(ooplrlist.hasOwnProperty(ooid)){
                            if(ooplrlist[ooid].state == 'paused'){
                                curr_progress = 2;
                            }
                        }
                    }
                }
                else if(currvideo.length > 0){
                    if(curr_progress){
                    }
                    else{
                        if(currvideo[0].paused == true){
                            curr_progress =2;
                        }
                    }
                }
                else if(curraudio.length > 0){
                    if(curr_progress){
                    }
                    else{
                        if(curraudio[0].paused == true){
                            curr_progress =2;
                        }
                    }
                }
            }
        }
        else if($scope.timer == true){
            $("#timer").countdown('pause');
            var time = parseInt($("#timer").text())
            var c_time = limit-time;
            if(c_time < limit){
                curr_progress = 2;
            }
            else if(c_time == limit){
                curr_progress = 1;
            }
            else{
            }
        }
        else{
        }
        if(curr_progress != undefined && ((curr_progress != r_progress && r_progress != 1) || r_progress == undefined)){
            var prog_dic = {}
            if(pid == undefined){
                p_id = 'c'+cid;
            }
            else if(pid == 'quiz')
            {
                p_id = 'q'+cid;
            }
            else{
                p_id = pid;
            }
            for(i=0;i<$scope.ombook['chapters'].length;i++){
                if($scope.ombook['chapters'][i]['cid'] == cid){
                    if(pid == undefined){
                        $scope.ombook['chapters'][i]['chap_progress'] = curr_progress;
                    }
                    else if(pid == 'quiz'){
                        $scope.ombook['chapters'][i]['quiz_progress'] = curr_progress;
                    }
                    else{
                        for(l=0;l<$scope.ombook['chapters'][i]['lpages'].length;l++){
                            if(pid == $scope.ombook['chapters'][i]['lpages'][l]['pid']){
                                $scope.ombook['chapters'][i]['lpages'][l]['progress'] = curr_progress;
                                break;
                            }

                        }
                    }
                    if(curr_progress == 1){
                        progress = progress + 1;
                        console.log(progress)
                    }
                    break;
                }
            }
            prog_dic['omb_id'] = $scope.ombook['id']
            prog_dic['batch_id'] = $scope.batch_id
            prog_dic['pid'] = p_id
            prog_dic['progress'] = curr_progress;
            prog_dic['total_pages'] = total_page;
            prog_dic['progress_pages'] = progress;
            //prog_dic[p_id] = curr_progress;
            progrss_list.push(prog_dic);
        }
        else{

        }
        if(progrss_list.length >= 5){
            var up_progress_dic = progrss_list.splice(-5);
            for(p=0;p<up_progress_dic.length;p++){
                k = up_progress_dic[p]['pid']
                $scope.om_update['ombook']['progress'][k] = up_progress_dic[p]['progress']
            }
            var data = {}
            data['ombook_id'] = $scope.ombook['id']
            data['response'] = $scope.om_update;
            onDeviceReady('ombookdetails',data,true);
            ombookservice.update_ombook_progress(JSON.stringify(up_progress_dic))
        }
    }

    $scope.toggleNav = function(){
        $('.omb-nav').toggleClass('hide')
    }
    
    $scope.close = function(){
        $ionicHistory.goBack()
    }
    $scope.goto =function(chapter,page_no,pid,progress_g,pop_page_no){
        c_page_no = pop_page_no;
        cid = chapter.cid
        next_chapter = cid;
         $ionicLoading.show({content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        $("#anotation_button").hide();
        $scope.VideoPause(cid,pid,page_no)
        $scope.update_progress(cid,pid,progress_g)
        if(page_no != undefined && chapter.lpages[page_no+1]){
            if(chapter.lpages[page_no].ptype == "4" || chapter.lpages[page_no].ptype == "9"){
                $('.omb-nav').removeClass('hide')
            } else {
                $('.omb-nav').addClass('hide')
            }
        }
        if(page_no >= 0){
            $scope.page_no = page_no;
            next_page = $scope.page_no
            $scope.chapter_no = $scope.ombook.chapters.indexOf(chapter);
            if(chapter['gate_questions']){
                $scope.gate_question = true;
            }else if(chapter['gate_questions']>0 && chapter['cleared'] == 1){
                $scope.gated = false;
            }else if(chapter['gate_questions']>0){
                alert("YOU NEED TO CLEAR THIS ASSESSMENT");
                $scope.gated = true;
            } else {
                $scope.gated = false;
            }
        } else {
            $scope.page_no = undefined;
            next_page = $scope.page_no
            $scope.chapter_no = $scope.ombook.chapters.indexOf(chapter);
        }
        if((next_page != undefined) && (next_page != chapter['lpages'].length)){
            if($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['atn'] != ''){
                $("#anotation_button").show();
            }
        }
        $scope.Page_creation();

        $("#timer").countdown('destroy')
        if($scope.page_no == undefined){
            $("#timer").countdown({until:limit,format:'S'})
            $scope.timer = true;
        }
        else if(($scope.page_no != undefined && ($scope.page_no != ($scope.ombook['chapters'][$scope.chapter_no]['lpages']).length)) && ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] == 4 || $scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] == 9) ){
            $scope.timer = false;
        }
        else if(($scope.page_no != ($scope.ombook['chapters'][$scope.chapter_no]['lpages']).length) && ($scope.page_no == undefined || ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] != 4) || ($scope.ombook['chapters'][$scope.chapter_no]['lpages'][$scope.page_no]['ptype'] != 9))){
            $("#timer").countdown({until:limit,format:'S'})
            $scope.timer = true;
        }
        else{
        }
        $timeout(function(){
            $ionicLoading.hide();
        },2000);
    }
    $scope.showannotation = function(){
        var ann_id = $('.active-page>.annotation').attr('id');
        //$('.active-page>.annotation').show();
        $('.active-page>.annotation').toggleClass('hide')
        $location.hash(ann_id)
        $anchorScroll();
    }
    $scope.$on('$ionicView.leave', function (event, viewData) {
        screen.unlockOrientation();
    })
    $scope.$on('$ionicView.enter', function (event, viewData) {
        //$ionicNavBarDelegate.showBar(false);
        screen.lockOrientation('landscape');
        if($rootScope.cleared == true){
            $scope.update_cid = $rootScope.update_ombook_id['cid']
            $scope.update_pid = $rootScope.update_ombook_id['pid']
            for(i=0;i<$scope.ombook['chapters'].length;i++){
            total_page = total_page + 1
            if($scope.update_cid == $scope.ombook['chapters'][i]['cid']){
                $scope.chapter_no = i;
                if($scope.update_pid == 'overview'){
                    $scope.page_no = undefined;
                }
                else if($scope.update_pid == 'quiz'){
                    $scope.page_no = $scope.ombook['chapters'][i]['lpages'].length;
                }
                else{
                    for(j=0;j<$scope.ombook['chapters'][i]['lpages'].length;j++){
                        if($scope.ombook['chapters'][i]['lpages'][j]['pid'] == $scope.update_pid){
                            $scope.page_no = j;
                            currPage = $('#chap-'+$scope.update_cid+'-page-'+$scope.update_pid)
                        }
                    }
                }
            }
        }

            $scope.start_ombook(true);
            $rootScope.cleared = '';
        }
        else{
        }
    });
    $scope.get_audiorecorder= function(obj){
  	    console.log(obj);
  	    console.log(obj.currentTarget)
  	    obj.currentTarget.remove();
  	    if(platform_name == 'Android'){
  	        var recorder_html = "<div class='row'><div class='col'><button class='button icon-left ion-mic-a' ng-click='start_recording($event)'>Record</button></div><div class='col'><button class='button icon-left ion-record' ng-click='stop_recording($event)'>Stop</button></div></div><div class='row'><div class='col'><button class='button' ng-click='play_recording($event)'>play</button></div><div class='col'><button class='button icon-left' ng-click='reset_recording($event)'>Reset</button></div></div><div class='row'><div class='col'><button class='button button-block button-positive' ng-click='save_recording()'>Save</button></div></div>";
  	    }
  	    else{
  	        var recorder_html = "<div class='row'><div class='col'><button class='button icon-left ion-mic-a' ng-click='start_recording($event)'>Record</button></div><div class='col'><button class='button icon-left ion-pause' ng-click='pause_recording($event)'>Pause</button></div><div class='col'><button class='button icon-left ion-record' ng-click='stop_recording($event)'>Stop</button></div></div><div class='row'><div class='col'><button class='button icon-left ion-star' ng-click='play_recording($event)'>play</button></div><div class='col'><button class='button icon-left' ng-click='resume_recording($event)'>resume</button></div><div class='col'><button class='button icon-left' ng-click='reset_recording($event)'>Reset</button></div></div><div class='row'><div class='col'><button class='button button-block button-positive' ng-click='save_recording()'>Save</button></div></div>";
  	    }
  	    $('.recorder').html($compile(recorder_html)($scope));
  	}

 function get_embed_code( page,ppwd,ppht){
  console.log("get_embed_code")
  ppwd = ppwd
  ppht = ppht
  url = page.url;
  local = page.local;
  cat = page.ptype;
  var isLocal = (parseInt(local) == 1);
  var embedCode = "";
  //cat  = fixCat(url, cat);
  if(cat == "2"){
     if(isLocal){
        //embedcode = image_embed_code(url, 0, ppht);
        //embedcode = image_embed_code(url, 0, 0);
        embedcode = image_embed_code(url, ppwd, ppht);
//        if (url.match(/\.svg/g)) {
//            embedcode = "<center><object type='image/svg+xml' class='img-responsive' style='max-width:" + ppwd +"px; max-height:" + ppht + "px; min-height:" + ppht + "px; min-width:" + ppwd + "px;' data='" + url +"' </></object></center>";
//        }
//        else{
//            embedcode = "<center><div><img class='img-responsive' style='max-width:" + ppwd +"px; max-height:" + ppht + "px; min-height:" + ppht + "px; min-width:" + ppwd + "px;' src='" + url +"' </></div></center>";
//        }

     }
     else{
        embedcode = html_embed_code(url, ppwd, ppht);
     }
  }
  else if(cat == "6"){
     if(isLocal){
        //embedcode = image_embed_code(url, 0, ppht);
        //embedcode = image_embed_code(url, 0, 0);
        embedcode = image_embed_code(url, ppwd, ppht);
     }
     else{
         if(checkimage(url)){
            //embedcode = image_embed_code(url, 0, ppht);
            //embedcode = image_embed_code(url, 0, 0);
            embedcode = image_embed_code(url, ppwd, ppht);
         }
         else{
           embedcode = html_embed_code(url, ppwd, ppht);
         }
     }
  }
  else if(cat == "4"){

     var vwd = ppwd; var vht = ppht;
     var page_height = parseInt(ppht);
     vht = page_height * .8;
     //vwd = vht * 4/3;
     if(url.search("ooyala.com") != -1){
        embedcode = ooyala_js_container(url);
     }
     else{
        if(isLocal){
           //embedcode = "<div class='html5_locv_wrapper video-container' style='background:black;color:white;cursor:pointer' onclick='javascript:initH5Plr(this);'  init='0' url='" + url + "' ><div style='margin-top:"+ 20+ "%'><center> <h1 style='color:white;'>" + page.ptitle + "</h1><i class='icon-youtube-play icon-round icon-round-lg icon-bg-red'></i></center></div></div>";
           embedcode = "<div class='video-container' ><video controls width='560' autoplay><source src='" + url + "'  type='video/mp4'>Your browser does not support this video format.</video></div>";
        }
        else{
        embedcode = video_embed_code(url, isLocal, vwd, vht);
        }
     }
  }
  else if(cat == "9"){
     var awd = ppwd;
     if(page.prac_id != ""){
       awd = .7 * ppwd;

     }
     embedcode = html5_audio_embed_code(url, awd);
     //embedcode = "<div class='html5_audio_wrapper' data-src='" + url + "'></div>";
  }
  else if(cat == "5"){
     //embedcode = page.html;
     embedcode = unescape(page.html);
  }
  else{
     embedcode = html_embed_code(url, ppwd, ppht);
  }
  var retcode = "";
  if(page.prac_id == ""){
       retcode = embedcode;
  }
   else{
      var ar = "<p>Play the reference clip above and practice. You can record your practice using the OM Riyaaz recorder below. When you are happy with your recording you can save it.</p><div class='recorder'><div ng-click='get_audiorecorder($event);' prac_id='" + page.prac_id +"'><img src='http://s3.amazonaws.com/live.shankarmahadevanacademy.com/portal/images/b_omriyaaz.png'></div></div>";
      retcode = "<div class='row'><div class='col col-40'><div class='prac_v' style='height:300px;'>" + embedcode + ar+"</div></div>";
       if(page.prac_ntn_type == "img"){
          retcode +=  "<div class='col col-60'><div style='overflow-y:auto;overflow-x:hidden;height:" +( page_height - 80) + "px;'><center><img  class='sma_prac_ntn' src='" + page.prac_ntn + "' /></center></div></div></div>";
       }
       else{
          retcode +=  "<div class='col col-60'>" + page.prac_ntn + "</div></div>";
       }
   }
   return retcode
}

    $scope.start_recording = function(obj){
        //var src = "myrecording.mp3";
        if(platform_name == 'Android'){
            $scope.file_name = 'practice_recording.amr';
        }
        else{
            $scope.file_name = 'practice_recording.wav';
        }
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs){
            fs.root.getFile($scope.file_name, {create: true, exclusive: false}, function(fileEntry) {
  			    src = fileEntry.toInternalURL();
  			    mediaRec = new Media(src,
            // success callback
            function() {
                console.log("recordAudio():Audio Success");
            },
            // error callback
            function(err) {
                console.log("recordAudio():Audio Error: "+ err.code);
            });
        // Record audio
        mediaRec.startRecord();
  			},function(){console.log('error')}
  		);
		}, function(){
		    console.log('ERROR')
		});
//        mediaRec = new Media(src,
//            // success callback
//            function() {
//                console.log("recordAudio():Audio Success");
//            },
//            // error callback
//            function(err) {
//                console.log("recordAudio():Audio Error: "+ err.code);
//            });
//        // Record audio
//        mediaRec.startRecord();
    }

    $scope.stop_recording = function(obj){
        mediaRec.stopRecord();
        mediaRec.release();
    }

    $scope.reset_recording = function(){
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs){
            fs.root.getFile($scope.file_name, {create: false}, function(fileEntry) {
                fileEntry.remove(function(){
                    console.log('The file has been removed succesfully');
                },function(error){
                    console.log('Error deleting the file');
                },function(){
                    console.log('The file doesnt exist');
                });
  		    });
		});
    }

    $scope.pause_recording = function(obj){
        mediaRec.pauseRecord();
    }

    $scope.resume_recording = function(obj){
        mediaRec.resumeRecord();
    }

    $scope.play_recording = function(obj){
        // Play the audio file at url
        var my_media = new Media(src,
            // success callback
            function () { console.log("playAudio():Audio Success"); },
            // error callback
            function (err) { console.log("playAudio():Audio Error: " + err); }
        );

        // Play audio
        my_media.play();

        // Pause after 10 seconds
//        setTimeout(function () {
//            my_media.pause();
//        }, 10000);
    }

    $scope.save_recording = function(){
        console.log('ajax to save the recording');
        if(typeof(src) == "undefined"){
            alert("record the voice and the save.");
            return true;
        }
        var win = function (r) {
            console.log("Code = " + r.responseCode);
            console.log("Response = " + r.response);
            console.log("Sent = " + r.bytesSent);
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs){
                fs.root.getFile($scope.file_name, {create: false}, function(fileEntry) {
                    fileEntry.remove(function(){
                        console.log('The file has been removed succesfully');
                    },function(error){
                        console.log('Error deleting the file');
                    },function(){
                        console.log('The file doesnt exist');
                    });
  		        });
		    });
        }

        var fail = function (error) {
            alert("An error has occurred: Code = " + error.code);
            console.log("upload error source " + error.source);
            console.log("upload error target " + error.target);
        }

        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = "practice_recording";
        if(platform_name == 'Android'){
            options.mimeType = "amr";
        }
        else{
            options.mimeType = "wav";
        }
        options.httpMethod = "POST"

        var params = {};
        params.session_id = $rootScope.session_id;
        pagedata = $('.active-page').data()
        page_id  = pagedata.pageId
        params.practice_id = page_id;

        options.params = params;

        var ft = new FileTransfer();
        var upload_url = ajax_domain+'/apis/upload/';
        ft.upload(src, encodeURI(upload_url), win, fail, options);
    }

});