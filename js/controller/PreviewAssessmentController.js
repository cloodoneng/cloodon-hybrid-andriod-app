app.controller('PreviewAssessmentController', function($scope,$http,global,$rootScope,$q,$window,assessmentservice,$ionicHistory){
    $rootScope.assessmentview = true;
    $scope.submitconfirmation = 'false';
    var id = $("#assessment_id").val();
    var state = $("#assessment_status").val();
    var assessmentdata;
    var q;
    var ppht = 200;
    var ppwd = 175;
    var dwd = 175;
    var qrid;
    $scope.assess_basic_details = global.assessment_intro;
    $scope.title = $scope.assess_basic_details['title']
    if($scope.assess_basic_details['settings']['25'] > 0){
        var time = 60 * ($scope.assess_basic_details['settings']['25'])
        var time_backup = 60 * ($scope.assess_basic_details['settings']['25'])
    }
    var promise = assessmentservice.get_assessment(id,state,time)
    promise.then(function(de) {
        if(de){
            var senddata = de['d']
            $scope.viewscopes(senddata)
            var state_db = de['state_db']
            var promise = assessmentservice.get_json_data(true,id,state,time,senddata,state_db)
            promise.then(function(data){
                //alert("success" + JSON.stringify(data));
                $scope.viewscopes(data);

            },function(reason){
                $scope.viewscopes(reason);
            });
        }
        else{
            var promise = assessmentservice.get_json_data(false,id,state,time);
            promise.then(function(data){
                $scope.viewscopes(data);
            },function(reason){
                $scope.viewscopes(reason);
            });
        }
    }, function(reason) {
        console.log('Failed: ' + reason);
        var promise = assessmentservice.get_json_data(false,id,state,time);
        promise.then(function(data){
            $scope.viewscopes(data);
        },function(reason){
            $scope.viewscopes(reason);
        });
    }, function(update) {
        console.log('Got notification: ' + update);
        //var promise = assessmentservice.get_json_data(false,id,state,time);
    });
    $scope.viewscopes = function(response){
        console.log(JSON.stringify(response))
        if(state == 'Evaluated'){
            $scope.evaluated = 'true';
        }
        else{
            $scope.evaluated = 'false';
        }
        //console.log(JSON.stringify(response))
        //$scope.show_hint = $scope.assess_basic_details['settings']['show_hint'];
        if (response['time_limit'] >= 0){
            time = response['time_limit']
            console.log(time)
            console.log(parseInt(time));
        }
        else{
            response['time_limit'] = time;
        }
        qrid = response['qrid'];
        var data = [];
        if(response['status'] == 1){
            $scope.failed = undefined;
            $scope.ass = response['quiz_json']
            console.log(JSON.stringify($scope.ass))
            var data = [];
            var total_score = 0;
            angular.forEach($scope.ass, function(value, key) {
                var temp = {};
                temp['question'] = JSON.parse(value['question']);
                temp['answer'] = JSON.parse(value['answer']);
                if(temp['question']['type'] == '5'){
                    if(temp['question']['options_B'] == undefined){
                        var option_b = temp['question']['options'].slice()
                        temp['question']['options_B'] = shuffle(option_b);
                        console.log(JSON.stringify(temp['question']['options_B']))
                        angular.forEach(temp['question']['options_B'], function(op_val, op_key){
                            if(op_val['selected'] == undefined){
                                op_val['selected'] = false;
                            }
                            else{
                            }
                            if(op_val['selected_op_index'] == undefined){
                               op_val['selected_op_index'] = '';
                            }
                            else{
                            }

                        });
                    }
                }
                var nm = temp['answer']['student_answers'];
                var s_ans = [];
                if(nm){
                    for(i=0; i<nm.length; i++){
                        s = JSON.parse(nm[i]);
                        s_ans.push(s);
                    }
                }
                //                if(temp['answer']['qid']){
                //                    $scope.evaluated = 'true';
                //                }
                //                else{
                //                    $scope.evaluated = 'false';
                //                }
                if(temp['answer'] != '{}'){
                    total_score = total_score + temp['answer']['score']
                }
                else{
                }
                $scope.total_marks = total_score;
                angular.forEach(temp['question']['options'], function(value, key){
                    console.log(value['student_ans']);
                    console.log("@@@@@@@@@@@@@@")
                    if(value['student_ans'] != undefined ){
                    }
                    else{
                        if((temp['question']['type'] == '1') || (temp['question']['type'] == '2')){
                            value['student_ans'] = false;
                        }
                        else{
                            value['student_ans'] = '';
                        }
                    }
                    angular.forEach(s_ans, function(valuest, key){
                        if(value['id'] == valuest['opt_id']){
                            temp['question']['options'][key]['std_ans'] = valuest['answer'];
                        }
                        else{
                        }
                    });
                });
                data.push(temp);
            });
            $scope.assessments = data;
            console.log(JSON.stringify($scope.assessments));
            $scope.question_index = 0 ;
            $scope.option = {};
        }
        else{
            $rootScope.assessmentview = false;
            $scope.assessments = undefined;
            $scope.failed = response['error_info'];
            $scope.evaluated = 'true';
        }
        //        if($scope.evaluated == 'true'){
        //            $("timer").countdown('pause');
        //        }
        //        if($scope.evaluated == 'false' && time == 0 && state == 'In Progress'){
        //           alert("Times up! submit quiz");
        //           $scope.submit('timesup');
        //        }
        if(state == 'Due'){
            time = time_backup;
        }
        $scope.$emit('eventName',$scope);
    }
    //    $scope.clock = function(){
    //        if($scope.evaluated == 'false'){
    //            $("#timer").countdown({until: time, compact: true, onExpiry:expiry});
    //            $("#timer_hidden").countdown({until: time,format:'S'});
    //        }
    //
    //    }
    $scope.$on('$ionicView.enter', function (event, viewData) {
        if($scope.evaluated == 'false'){
            $("#timer").countdown({until: time, compact: true, onExpiry:expiry});
            $("#timer_hidden").countdown({until: time,format:'S'});
        }
        if($scope.evaluated == 'false' && time == 0){
            alert("Times up! submit quiz");
            $scope.submit('timesup');
        }
    });
//    $scope.random = function() {
//        return 0.5 - Math.random();
//    }
    $scope.update_match = function(match_option,ans){
        console.log("###############################")
        console.log(match_option)
        console.log(match_option['student_ans'])
        console.log(match_option['student_prev_ans'])
        console.log(ans);
        op_index = match_option['index']-1;
        console.log(op_index)
        if(ans == ''){
            ans = match_option['student_prev_ans']
        }
//        $scope.assessments[$scope.question_index]['question']['options'][op_index]['student_answer'] = ans;
        for(op_b=0;op_b<$scope.assessments[$scope.question_index]['question']['options_B'].length;op_b++){
            if($scope.assessments[$scope.question_index]['question']['options_B'][op_b]['id'] == ans){
                if($scope.assessments[$scope.question_index]['question']['options_B'][op_b]['selected'] == true){
                    $scope.assessments[$scope.question_index]['question']['options'][op_index]['student_prev_ans'] = '';
                    $scope.assessments[$scope.question_index]['question']['options_B'][op_b]['selected'] = false;
                    $scope.assessments[$scope.question_index]['question']['options_B'][op_b]['selected_op_index'] = '';
                }
                else{
                    if($scope.assessments[$scope.question_index]['question']['options'][op_index]['student_prev_ans'] == '' || $scope.assessments[$scope.question_index]['question']['options'][op_index]['student_prev_ans'] == undefined){
                        $scope.assessments[$scope.question_index]['question']['options'][op_index]['student_prev_ans'] = ans;
                        $scope.assessments[$scope.question_index]['question']['options_B'][op_b]['selected'] = true;
                        $scope.assessments[$scope.question_index]['question']['options_B'][op_b]['selected_op_index'] = op_index;
                    }
                    else{
                        reset_id = $scope.assessments[$scope.question_index]['question']['options'][op_index]['student_prev_ans']
                        for(opt_b=0;opt_b<$scope.assessments[$scope.question_index]['question']['options_B'].length;opt_b++){
                            if($scope.assessments[$scope.question_index]['question']['options_B'][opt_b]['id'] == reset_id){
                                $scope.assessments[$scope.question_index]['question']['options_B'][opt_b]['selected'] = false;
                                $scope.assessments[$scope.question_index]['question']['options_B'][opt_b]['selected_op_index'] = '';
                            }
                        }
                        $scope.assessments[$scope.question_index]['question']['options'][op_index]['student_prev_ans'] = ans;
                        $scope.assessments[$scope.question_index]['question']['options_B'][op_b]['selected'] = true;
                        $scope.assessments[$scope.question_index]['question']['options_B'][op_b]['selected_op_index'] = op_index;
                    }
                }
                console.log(JSON.stringify($scope.assessments[$scope.question_index]['question']['options_B'][op_b]))
                console.log(JSON.stringify($scope.assessments[$scope.question_index]['question']['options'][op_index]))
                break;
            }
        }
    }
    $scope.submit = function(data){
        $scope.submitconfirmation = 'true';
        var assmt_ans = [];
        var snddata = {};
        var q_index = [];
        angular.forEach($scope.assessments, function(value, keys) {
            var que_ans = {};
            var student_answers = [];
            var bol = "false"
            angular.forEach(value['question']['options'], function(value, key) {
                console.log(JSON.stringify(value))
                var opt = {};
                if(($scope.assessments[keys]['question']['type'] == 1) || ($scope.assessments[keys]['question']['type'] == 2)){
                    if(bol == "false"){
                        if((value['student_ans'] == true) || (value['student_ans'] != '')){
                            bol = "true";
                        }
                        else{
                            bol = "false";
                        }
                    }
                }
                else if($scope.assessments[keys]['question']['type'] == 4 || $scope.assessments[keys]['question']['type'] == 8 || $scope.assessments[keys]['question']['type'] == 5){
                    if(value['student_ans'] != ''){
                        bol = 'true'
                    }
                    else{
                        bol = 'false'
                    }
                }
                else if($scope.assessments[keys]['question']['type'] == 3){
                    if(bol == "false"){
                        if(value['index'] == (key+1)){
                            bol = "false";
                        }
                        else{
                            bol = "true";
                        }
                    }
                }
                if(value['student_ans'] == true){
                    opt['answer'] = "true"
                }
                else if((value['student_ans'] == false)){
                    opt['answer'] = "false"
                }
                else if((value['student_ans'] != true) && (value['student_ans'] != false)){
                    opt['answer'] = value['student_ans'];
                }
                else{
                    opt['answer'] = ''
                }
                opt['opt_id'] = value['id']
                student_answers.push(JSON.stringify(opt));
            });

            console.log(JSON.stringify(student_answers))
            que_ans['qid'] = value['question']['id'];
            que_ans['answers_json'] = student_answers;
            assmt_ans.push(que_ans);
            console.log(JSON.stringify(assmt_ans))
            if(bol == "false"){
                q_index.push(keys+1);
            }
        });
        $scope.question_no = q_index;
        snddata['records'] = JSON.stringify(assmt_ans);
        snddata['qrid'] = qrid;
        assessmentdata = snddata;
        if(data == 'timesup'){
            $scope.final_submission();
        }
    };
    $scope.final_submission = function(){
        $("#timer").countdown('pause');
        $("#timer_hidden").countdown('pause');
        $.ajax({
            type: "POST",
            url: ajax_domain +'/apis/user-assessment-submit/',
            data: assessmentdata,
            dataType:'json',
            timeout : 70000,
            error : function(){
                var ans = confirm("Oops Some Error occured.Do you want to save and resume latter? ")
                if(ans == true){
                    $scope.save_resume();
                    $ionicHistory.goBack()
                }
                else{
                    $ionicHistory.goBack()
                }

            },
            success : function(response){
                $rootScope.reload_course_detail = true;
                $ionicHistory.goBack(-2)
                //assessmentservice.get_json_data(true,id,'Evaluated','',undefined,'In Progress')
            }
        })
    };
    $scope.assback = function(){
        $rootScope.assessmentview = false;
        ($rootScope.history).splice(-1);
        onBackKeyDown({"back_opn":"app"});
    }
    $scope.dismissmodal = function(){
        $("#timer").countdown('resume');
    }
    $scope.video = function(url,isLocal){
        video_content = video_embed_code(url,isLocal,ppwd,ppht)
        $("#embeded_video").append(video_content);
    };
    $scope.audio = function(url){
        audio_content = html5_audio_embed_code(url, ppwd);
        $("#embeded_audio").append(audio_content);
    };
    $scope.image = function(url,isLocal){
        if(isLocal){
            image_content =image_embed_code(url, dwd, ppht);
        }
        else{
            if(checkimage(url)){
                image_content = image_embed_code(url, dwd, ppht);
            }
            else{
                image_content = html_embed_code(url, ppwd, ppht);
            }
        }
        $("#embeded_image").append(image_content);
    };
    $scope.next = function () {
        if ($scope.question_index == $scope.assessments.length-1) {
            $scope.question_index++;
            $scope.submit();
        }
        else {
            $scope.question_index++;
        }
    };
    $scope.previous = function (id) {
        var ind = $scope.question_index;
        if(id == 'submitconfirmation' && $scope.submitconfirmation == 'true'){
            len = ($scope.assessments).length
            $scope.submitconfirmation = 'false';
            if(ind == len){
                ind = ind -1
                $scope.question_index = ind;
            }
            else{
                $scope.question_index = ind;
            }
        }
        else{
            if ($scope.question_index == 0) {
                $scope.question_index = 0;
            }
            else {
                ind = ind -1
                $scope.question_index = ind;
            }
        }
    };
    $scope.unattemptedquestion = function(q_id){
        $scope.submitconfirmation = 'false';
        q = q_id-1;
        $scope.question_index = q_id-1;
        $("#timer").countdown('resume')
    };
    $scope.save_resume = function(save_button,obj){
        $("#timer").countdown('pause');
        $("#timer_hidden").countdown('pause');
        var hidden_time = $('#timer_hidden span .countdown_amount').html()
        time = parseInt(hidden_time);
        var save_assessment = [];
        angular.forEach($scope.assessments, function(value, keys) {
            var temprary = {};
            temprary['question'] = JSON.stringify(value['question']);
            temprary['answer'] = JSON.stringify(value['answer']);
            save_assessment.push(temprary);
        });
        console.log(JSON.stringify($scope.assessments));
        var data = {};
        var save_ass = {};
        save_ass['status'] = 1;
        save_ass['quiz_json'] = save_assessment;
        save_ass['qrid'] = qrid;
        save_ass['time_limit'] = time;
        data['response'] = save_ass;
        data['qrid'] = id;
        data['state'] = state;
        onDeviceReady('assessmentdetails',data,true)
        if(save_button == true){
            $ionicHistory.goBack(-1)
        }
    };
    /*
    $scope.historyGoBack = function(){
        if($scope.evaluated == "false"){
             var answer = confirm("Do you want to save and resume?");
             if(answer == true){
                $scope.save_resume(false);
                $ionicHistory.goBack()
             }
             else{
             }
        }
        else{
            $ionicHistory.goBack()
        }
    }*/
    $scope.updateSelection = function(position, options) {
        angular.forEach(options, function(value, index) {
            if (position != index)
                value.student_ans = false;
        });
    };
    function onBackKeyDown(obj) {
        $rootScope.back(obj);
    }
    function expiry(){
    hasExpired = 1;
    alert("Times up! submit quiz");
    $scope.submit('timesup');
}
$scope.moveItem = function(optn, fromIndex, toIndex) {
    //Move the item in the array
    $scope.assessments[$scope.question_index ]['question']['options'].splice(fromIndex, 1);
    $scope.assessments[$scope.question_index ]['question']['options'].splice(toIndex, 0, optn);
    console.log(JSON.stringify($scope.assessments[$scope.question_index ]['question']['options']))
  };
});

