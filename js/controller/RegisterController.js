app.controller('RegisterController', function($scope,$http,$rootScope,$ionicLoading){
   $scope.register = {
   	username : "",
   	password: "",
   	firstname: "",
   	lastname : "",
   }

   $scope.logo = logo
   $scope.error_msg = "";
   $scope.signUp = function(registerForm){
   	data = $scope.register
   	if(registerForm.$valid){
   		$ionicLoading.show({content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
	   	$.ajax({
	      	    	type: 'POST',
	      		url:ajax_domain + '/apis/register/',
	      		crossDomain: true,
	       		data: data,
	       		dataType: 'jsonp',
	       		success: function(response, textStatus, jqXHR) {
	       			$ionicLoading.hide()
	       			if (response.status == 1) {
	       				user_credential = {username:$scope.register.username,password:$scope.register.password,'logged_in':true}
   					$rootScope.shouldShow = true;
				    	onDeviceReady('usercredential',user_credential,false);
				    	$rootScope.session_id = response['session_id']
   					$rootScope.user = response
    					window.location = "#myhome";
	       			} else {
	       				$scope.error_msg = response.error_description
	       			}
	       		},
	       		error: function (responseData, textStatus, errorThrown) {
			       alert('POST failed.');
			}
		});
   	}
    }
    $scope.social_login = function(url){
   	    try {    
            var ref = window.open(encodeURI(ajax_domain+url), '_blank', 'location=yes,scrollbars=yes,status=yes,clearsessioncache=yes,clearcache=yes');
            // var ref = cordova.InAppBrowser.open('http://demo.cloodon.com/apis/user-assessments/', '_blank', 'location=yes');
            ref.addEventListener('loadstart', function(event) { 
               ref.executeScript({ code: "localStorage.setItem( 'user', '' );" });
            });
            ref.addEventListener('loadstop', function(event) {
                console.log('stop: ' + event.url);
                ref.executeScript(
                    { code: "localStorage.getItem( 'user' );" },
                    function( values ) {
                        console.log(values);
                        if(values[0] != ""){
                            var data = JSON.parse(values[0])
                            console.log(data)
                            if(data.error == ""){
                			user_credential = {username:data.username,password:'!','logged_in':true}
                			$rootScope.shouldShow = true;
                			onDeviceReady('usercredential',user_credential,false);
                			$rootScope.session_id = data.session_id
                			$rootScope.user = response
                			window.location = "#myhome";
                            }else{
                            	$scope.error_msg = data.error;
                            }
                            ref.close()
                        }
                        }
                );
            });
            ref.addEventListener('exit', function(event) { console.log('exit:' + event.type); });
   	    } catch(err){
   	        console.log(err.message)
   	    }
   	}
});