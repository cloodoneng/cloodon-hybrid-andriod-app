function onDeviceReady(tbname,data,update,save) {
	console.log(data);
	var update;
    var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 3145728 );
	if(tbname == 'assessmentlists'){
	    var data = JSON.stringify(data);
		db.transaction(function(tx){
			if(update == false){
				//tx.executeSql('DROP TABLE IF EXISTS assessmentlists');
				console.log("create")
				tx.executeSql('CREATE TABLE IF NOT EXISTS assessmentlists (id unique, response)');
				tx.executeSql('INSERT INTO assessmentlists (id, response) VALUES (1, ?)',[data]);
			}
			else{
				console.log("update")
				tx.executeSql('UPDATE assessmentlists SET response = ? WHERE id=1',[data]);
			}
		}, function (err) {
			console.log("Error processing SQL: "+err.code);
		}, function() {
			console.log("success");
		});
	}
	else if(tbname == 'updateprogress'){
		console.log("updateprogress")
		var res = JSON.stringify(data);
		db.transaction(function(tx){
			console.log("inside")
			console.log(update);
			if(update == false){
				console.log("create")
				tx.executeSql('DROP TABLE IF EXISTS updateprogress');
				tx.executeSql('CREATE TABLE IF NOT EXISTS updateprogress (id unique,progress_data)');
				tx.executeSql('INSERT INTO updateprogress (id, progress_data) VALUES (1, ?)',[res]);
			}
			else{
				console.log('update')
				tx.executeSql('UPDATE updateprogress SET  = ? WHERE id=1',[res]);
			}
		}, function (err) {
			console.log("Error processing SQL: "+err.code+err.message);
		}, function() {
			console.log("success");
			var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
			db.transaction(function queryDB(tx) {
				//alert("querydb")
				tx.executeSql('SELECT * FROM updateprogress where id= 1',[],function(tx, results) {
					//alert("querysuccess")
					console.log(JSON.stringify(results));
					var len = results.rows.length;
					console.log("updateprogress table: " + len + " rows found.");
					for (var i=0; i<len; i++){
						console.log("Row = " + i + " ID = " + results.rows.item(i).id +" Data =  " + results.rows.item(i).progress_data);
					}
				}, function(err) {
					//alert("error")
					console.log("Error processing SQL: "+err.code);
				});
			}, function (err) {
				//alert("error")
				console.log("Error processing SQL: "+err.code);
			});
		});
	}
	else if(tbname == 'coursedetail'){
	    var response = JSON.stringify(data['response']);
	    var id = data['batch_id'];
	    console.log(data);
	    console.log(id)
		db.transaction(function(tx){
			if(update == false){
				//tx.executeSql('DROP TABLE IF EXISTS assessmentlists');
				console.log("create")
				tx.executeSql('CREATE TABLE IF NOT EXISTS coursedetail (batch_id unique, response)');
				tx.executeSql('INSERT INTO  coursedetail (batch_id, response) VALUES (?, ?)',[id,response]);
			}
			else{
				console.log("update")
				tx.executeSql('UPDATE coursedetail SET response = ? WHERE batch_id = ?',[response,id]);
			}
		}, function (err) {
			console.log("Error processing SQL: "+err.code);
		}, function() {
			console.log("success outer success loop");
			var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
			db.transaction(function queryDB(tx) {
				//alert("querydb")
				tx.executeSql('SELECT * FROM coursedetail', [], function(tx, results) {
					//alert("querysuccess")
					console.log(JSON.stringify(results));
					var len = results.rows.length;
					console.log("coursedetail table: " + len + " rows found.");
					for (var i=0; i<len; i++){
						console.log("Row = " + i + " ID = " + results.rows.item(i).batch_id + " Data =  " + results.rows.item(i).response);
					}
				}, function(err) {
					//alert("error")
					console.log("Error processing SQL: "+err.code);
				});
			}, function (err) {
				//alert("error")
				console.log("Error processing SQL: "+err.code);
			});
		});
	}
	else if(tbname == 'ombookdetails'){
	    var response = JSON.stringify(data['response']);
	    var id = data['ombook_id'];
	    var vers = data['version']
	    console.log(data);
	    console.log(id)
		db.transaction(function(tx){
			if(update == false){
				//tx.executeSql('DROP TABLE IF EXISTS assessmentlists');
				console.log("create")
				tx.executeSql('CREATE TABLE IF NOT EXISTS ombookdetails (ombook_id unique, response,version)');
				tx.executeSql('INSERT INTO  ombookdetails (ombook_id, response,version) VALUES (?, ? ,?)',[id,response,vers]);
			}
			else{
				console.log("update")
				if(vers == undefined){
					tx.executeSql('UPDATE ombookdetails SET response = ? WHERE ombook_id = ?',[response,id]);
				}
				else{
					tx.executeSql('UPDATE ombookdetails SET response = ?,version =? WHERE ombook_id = ?',[response,vers,id]);
				}
			}
		}, function (err) {
			console.log("Error processing SQL: "+err.code);
		}, function() {
			console.log("success outer success loop");
			var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
			db.transaction(function queryDB(tx) {
				//alert("querydb")
				tx.executeSql('SELECT * FROM ombookdetails where ombook_id= ?', [id], function(tx, results) {
					//alert("querysuccess")
					console.log(JSON.stringify(results));
					var len = results.rows.length;
					console.log("ombookdetails table: " + len + " rows found.");
					for (var i=0; i<len; i++){
						console.log("Row = " + i + " ID = " + results.rows.item(i).ombook_id + "version=" +results.rows.item(i).version + " Data =  " + results.rows.item(i).response);
					}
				}, function(err) {
					//alert("error")
					console.log("Error processing SQL: "+err.code);
				});
			}, function (err) {
				//alert("error")
				console.log("Error processing SQL: "+err.code);
			});
		});
	}
	else if(tbname == "assessmentdetails"){
		var qrid = data['qrid'];
		var response = JSON.stringify(data['response']);
		var state = data['state'];
		db.transaction(function(tx){
			if(update == false){
				tx.executeSql('CREATE TABLE IF NOT EXISTS assessmentdetails (qrid unique, response,status)');
				tx.executeSql('INSERT INTO assessmentdetails (qrid, response,status) VALUES (?, ?,?)',[qrid,response,state]);
			}
			else{
				console.log("else assessmentdetails")
				tx.executeSql('UPDATE assessmentdetails SET response = ?, status=? WHERE qrid=?',[response,state,qrid]);
			}
		}, function (err) {
			console.log("Error processing SQL: "+err.code);
			glob = false;
			console.log(glob)
			//console.log(save);

		}, function() {
			console.log("success");
			glob = true;
			console.log(glob);

		});
	}
	else if(tbname == "usercredential"){
		var data = JSON.stringify(data);
		db.transaction(function(tx){
			if(update == false){
				tx.executeSql('DROP TABLE IF EXISTS usercredential');
				tx.executeSql('CREATE TABLE IF NOT EXISTS usercredential (id unique,data)');
				tx.executeSql('INSERT INTO usercredential (id, data) VALUES (1, ?)',[data]);
			}
			else{
				tx.executeSql('UPDATE usercredential SET data = ? WHERE id=1',[data]);
			}
		}, function (err) {
			console.log("Error processing SQL: "+err.code);
		}, function() {
			console.log("success");
		});
	}
	else if(tbname == "coursecatalogdetail"){
		var data = JSON.stringify(data);
		db.transaction(function(tx){
			if(update == false){
				tx.executeSql('CREATE TABLE IF NOT EXISTS coursecatalogdetail (id unique,data)');
				tx.executeSql('INSERT INTO coursecatalogdetail (id, data) VALUES (1, ?)',[data]);
			}
			else{
				tx.executeSql('UPDATE coursecatalogdetail SET data = ? WHERE id=1',[data]);
			}
		}, function (err) {
			console.log("Error processing SQL: "+err.code);
		}, function() {
			console.log("success");
			var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
			db.transaction(function queryDB(tx) {
				tx.executeSql('SELECT * FROM coursecatalogdetail', [], function(tx, results) {
					console.log(JSON.stringify(results));
					var len = results.rows.length;
					console.log("coursecatalogdetail table: " + len + " rows found.");
					for (var i=0; i<len; i++){
						console.log("Row = " + i + " ID = " + results.rows.item(i).id + " Data =  ");
					}
				}, function(err) {
					console.log("Error processing SQL: "+err.code);
				});
			}, function (err) {
				console.log("Error processing SQL: "+err.code);
			});
		});
	}
	else if(tbname == "courselist"){
		var data = JSON.stringify(data);
		db.transaction(function(tx){
			if(update == false){
				tx.executeSql('CREATE TABLE IF NOT EXISTS courselist (id unique,course_list)');
				tx.executeSql('INSERT INTO courselist (id, course_list) VALUES (1, ?)',[data]);
			}
			else{
				tx.executeSql('UPDATE courselist SET course_list = ? WHERE id=1',[data]);
			}
		}, function (err) {
			console.log("Error processing SQL: "+err.code);
		}, function() {
			console.log("success");
		});
	}
	else if(tbname == "customerdata"){
		var data = JSON.stringify(data);
		db.transaction(function(tx){
			if(update == false){
				tx.executeSql('DROP TABLE IF EXISTS customerdata');
				tx.executeSql('CREATE TABLE IF NOT EXISTS customerdata (id unique,customer_json)');
				tx.executeSql('INSERT INTO customerdata (id, customer_json) VALUES (1, ?)',[data]);
			}
			else{
				tx.executeSql('UPDATE customerdata SET customer_json = ? WHERE id=1',[data]);
			}
		}, function (err) {
			console.log("Error processing SQL: "+err.code);
		}, function() {
			console.log("success");
		});
	}
	else{
	}
}