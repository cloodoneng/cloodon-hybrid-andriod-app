var video_state;
function html5_video_embed_code(url, width, height){
 return "<div class='resp-container video-container' ><video  onended='audio_end()' controls width='" + width + "' height='"+ height +"'><source src='" + url + "'  type='video/mp4'>Your browser does not support this video format.</video></div>";
}
function html5_audio_embed_code(url, width){
 //return "<audio controls  style='width:" + width + "px'><source src='" + url + "' >Your browser does not support this audio format.</audio>";
 if(url.search(".flv") != -1){
    return  "<div><center><object    data='http://releases.flowplayer.org/swf/flowplayer-3.2.18.swf' type='application/x-shockwave-flash' class=''>  <param value='playerID=audioplayer1&amp;soundFile=" + url  + "' name='FlashVars'><param value='high' name='quality'><param value='false' name='menu'> <param value='transparent' name='wmode'></object></center></div>";
 }
 return "<center><div><audio controls style='width:100%' onended='audio_end()'><source src='" + url + "'  type='audio/mp3'>Your browser does not support this audio format.</audio></div></center>"
}

function extractSubstr(str, key, endDelimiter){
   var indx = str.indexOf(key);
   var extractedValue = "";
   if( indx != -1){
      var indx2 = str.indexOf(endDelimiter, indx)
         var ln = key.length;
      if(indx2 != -1){
         extractedValue = str.substr(indx + ln, indx2 - (indx + ln));
      }
      else{
         extractedValue = str.substr(indx + ln);
      }
   }
   return extractedValue
}

var yt_embed_plr_id = 1;
function youtube_embed_code(url, width, height, autoplay){
   //http://www.youtube.com/v/di9D6BySso4?version=3&start=60&end=120&autoplay=0&hl=en_US&rel=0
   //https://www.youtube.com/watch?v=pfHxl46KyZM&list=PLSrJUYHlUbysuvzi5QSp9WKCaUT6uVmnn&index=2&t=119s
   //alert(url);
   yt_embed_plr_id++;
   if (url.indexOf('/embed/') >= 0)
        return "<div class='embed-responsive embed-responsive-16by9 video-container' ><iframe class='ytplr embed-responsive-item' width='560' height='315' id='player" + yt_embed_plr_id +"'  src='"+ url +"' frameborder='0' allowfullscreen></iframe></div>";
        //return "<div class='embed-responsive embed-responsive-16by9' ><iframe class='ytplr embed-responsive-item'  id='player" + yt_embed_plr_id +"'  src='"+ url +"' frameborder='0' allowfullscreen></iframe></div>";
   var vid = "";
   var startPos = "";
   var endPos = "";
   var code = "";
   vid = extractSubstr(url, 'watch?v=', '&');
   console.log(vid);
   if(vid == ""){
      vid = extractSubstr(url, '/v/', '?');
      //alert(vid);
   }
   if(vid != ""){
         startPos = extractSubstr(url, 'start=', '&');
         endPos = extractSubstr(url, 'end=', '&');
         //alert(startPos);
         //alert(endPos);
  }
   if(vid != ""){
      var src='http://www.youtube.com/embed/'+ vid +'?enablejsapi=1&rel=0&html5=1';
      console.log(src);
      if(startPos != ""){
         src = src + '&start=' + startPos;
         //alert(src);
      }
      if(endPos != ""){
         src = src + '&end=' + endPos;
         //alert(src);
      }
      if(typeof autoplay != 'undefined'){
         if(src.indexOf("?") != "-1"){
            src = src + "&autoplay=1";
            //alert(src);
         }
         else{
            src = src + "?autoplay=1";
            //alert("else");
            //alert(src);
         }
      }
      //var code = "<div class='resp-container' style='position:relative;height:"+"'><iframe class='ytplr' width='" + width + "' height='"+ height +"' id='player" + yt_embed_plr_id +"'  src='"+ src +"' frameborder='0' allowfullscreen></iframe></div>";
      var code = "<div class='video-container'><iframe class='ytplr' width='560' height='315' id='player" + yt_embed_plr_id +"'  src='"+ src +"' frameborder='0' allowfullscreen></iframe></div>";
      //var code = "<div class='resp-container' ><iframe class='ytplr' width='" + width + "' height='"+ height +"' id='player" + yt_embed_plr_id +"'  src='"+ src +"' frameborder='0' allowfullscreen></iframe></div>";
   }
   return code;
}

function html_embed_code(url, width, height){
  var height = $(window).height() * .8;
  width = "100%";
  return "<div class='embed-container'><div class='embed-overlay'></div><center><iframe frameborder='0' scrolling='yes' width='" + width + "' height='"+ height + "px' src='" + url + "' seamless >Unable to display as iframe is not supported</iframe></center></div>";
}

var oo_embed_plr_id = 1;
var ooplrlist = {};
function ooyala_embed_code(url, width, height){
   width=560;
   //height=349;
   height=315;
   //var tags = url.split("embedCode=");
   //var embedCode = tags[1];
   var embedCode = url.split("embedCode=")[1].split("&")[0];
   oo_embed_plr_id++;
   //return "<div class='ooplr resp-container'><iframe width='"+ width + "' height='" + height + "' src='http://player.ooyala.com/iframe.html?ec=" + embedCode + "&pbid=ODlmZDA4NjIyZmViMzQyM2QzZDMzZTlh&playerId=ooplr"+ oo_embed_plr_id +"' frameborder='0' allowfullscreen></iframe></div>";
   return "<div class='ooplr resp-container video-container'><iframe width='"+ width + "' height='" + height + "' src='http://player.ooyala.com/iframe.html?ec=" + embedCode + "&pbid=ODlmZDA4NjIyZmViMzQyM2QzZDMzZTlh&playerId=ooplr"+ oo_embed_plr_id +"' frameborder='0' allowfullscreen></iframe></div>";
}

function ooyala_js_container(url){
   var embedCode = url.split("embedCode=")[1].split("&")[0];
   oo_embed_plr_id++;
   return "<div class='ooplr video-container' ooid='oojs_player_" + oo_embed_plr_id + "' id='oojs_container_" + oo_embed_plr_id + "' code='" + embedCode + "'></div>";
}

function init_oo_players(ooyala_page_height){
    console.log("init");
    console.log(ooyala_page_height);
    $(".ooplr").each(function (){
        console.log("data");
          var ooid = $(this).attr("ooid");
          var container = $(this).attr("id");
          var embedCode = $(this).attr("code");
          var width =  '100%';
          if(ooyala_page_height){
            if($(this).closest('.tkslide').find('.prac_v').length){
              var height = '90%';
            }
            else{
              var height = ooyala_page_height * 3/4;
            }
          }
          else{
            var height = '90%';
          }
          ooplrlist[ooid] =  OO.Player.create(container, embedCode, {onCreate : window.onCreate, width: width, height: height });
    });

}

function pause_oo_player(key){
  if(key in ooplrlist){

     ooplrlist[key].pause();
  }
}

function toggle_oo_player(key){
  if(key in ooplrlist){
     var state = ooplrlist[key].getState();
     if(state == 'ready' || state == 'paused'){
       ooplrlist[key].play();
     }
     else{
       ooplrlist[key].pause();
     }
  }
}

function extract_youtubevid(url) {
  var video_id = "";
  if(url.indexOf('v=')!= -1)
  {
    video_id = url.split('v=')[1];
  }
  else if(url.indexOf('v/')!= -1)
  {
    video_id = url.split('v/')[1];
  }
  else if (url.indexOf('http://youtu.be/') != -1)
  {
    video_id = url.substring(16);
    return video_id;
  }
  else
  {
    return video_id;
  }
  var ampersandPosition = video_id.indexOf('&');
  if(ampersandPosition != -1) {
    video_id = video_id.substring(0, ampersandPosition);
  }
  return video_id;

}

function video_embed_code(url, isLocal, width, height, autoplay){
   console.log(url);
   console.log(isLocal);
   if(url.search("youtube.com") != -1 || url.search("youtu.be") != -1){
      console.log("youtube video if");
      return youtube_embed_code(url, width, height);
   }
   else if(url.search("ooyala.com") != -1){
      console.log("ooyala video else if");
      return ooyala_embed_code(url, width, height);
   }
   else if(url.search("vimeo.com") != -1){
      console.log("vimeo video else if")
      return vimeo_embed_code(url, width, height);
   }
   else if(isLocal == "1"){
      return html5_video_embed_code(url, width, height);
   }
   else{
      return "<iframe src='"  + url  +"' width='" + width + "' height='"+ height +"' >This video format is not supported</iframe>";
   }
}

function extract_vimeovid(url){
  url_parts = url.split('/');
  return url_parts[(url_parts.length-1)];
}


function image_embed_code(url, width, height){
   if (url.match(/\.svg/g)) {
      return "<div class='embed-container'><div class='embed-overlay'></div><center><object type='image/svg+xml' class='img-responsive' class='img-responsive' data='" + url +"' </></object></center></div>";
   } else if(typeof width !== 'undefined' && width != 0){
      return "<center><div><img class='img-responsive' src='" + url +"' </></div></center>";
   } else if(typeof height !== 'undefined' && height != 0){
      return "<center><div><img class='img-responsive'  src='" + url +"' </></div></center>";
   } else{
      return "<center><img class='img-responsive' src='" + url +"' </></center>";
   }
}
//overwritten image_embed_code
/*function image_embed_code(url,width,height){
    if (url.match(/\.svg/g)) {
        return "<center><object type='image/svg+xml' class='img-responsive' style='max-width:" + width +"px; max-height:" + height + "px; min-height:" + height + "px; min-width:" + width + "px;' data='" + url +"' </></object></center>";
    }
    else if(width != 0 || height != 0){
        return "<center><div><img class='img-responsive' style='max-width:" + width +"px; max-height:" + height + "px; min-height:" + height + "px; min-width:" + width + "px;' src='" + url +"' </></div></center>";
    }
    else{
        return "<center><img class='img-responsive' src='" + url +"' </></center>";
    }
}*/

function checkimage(url){
   var imgexten = ['png', 'jpg', 'jpeg', 'gif', 'tiff'];
   for(var k = 0; k < imgexten.length; k++){
      if( url.toLowerCase().search("." + imgexten[k]) != -1){
           return true;
      }
   }
   return false;
}

var ooyalaPlayer_id = 1;
function get_ooyala_objectcode(url,height,width)
{
   var tags = url.split("embedCode=");
   var embedcode = tags[1];
   if (height == "" ) height = 350;
   if (width == "" ) width = 480;
   ooyalaPlayer_id += 1;
   //var html = '<div class="resp-container"><object class="img-responsive" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="ooyalaPlayer' + ooyalaPlayer_id + '" width="' + width +'" height="' + height + '"';
   var html = '<div class="resp-container"><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="ooyalaPlayer' + ooyalaPlayer_id + '"';

   html += ' codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">';
   html += '<param name="movie" value=" http://player.ooyala.com/player.swf" />';
   html += '<param name="bgcolor" value="#000000" />';
   html += '<param name="allowScriptAccess" value="always" />';
   html += '<param name="allowFullScreen" value="true" />';
   html += '<param name="wmode" value="transparent" />';
   html += '<embed src="http://player.ooyala.com/player.swf" bgcolor="#000000" width="' + width +'" height="' + height + '"';
  // html += '<embed src="http://player.ooyala.com/player.swf" bgcolor="#000000"';
   html += ' name="ooyalaPlayer"';
   html += ' align="middle" play="true" loop="false"';
   html += ' allowscriptaccess="always" type="application/x-shockwave-flash"';
   html += ' allowfullscreen="true" wmode="transparent" ';
   html += ' flashvars="embedCode='+ embedcode +'"';
   html += ' pluginspage="http://www.adobe.com/go/getflashplayer">';
   html += '</embed>';

   html += '<param name="flashvars" value="embedCode='+ embedcode +'" />';
   //html += '<embed src="http://player.ooyala.com/player.swf" bgcolor="#000000" width="' + width +'" height="' + height + '"';
   html += '</object></div>';
   return html;

}

function video_embed_code_url(url, isLocal, width, height){
   if(url.search("youtube.com") != -1){
      vid = extract_youtubevid(url);
      src='http://www.youtube.com/embed/'+ vid +'?enablejsapi=1&rel=0&html5=1';
      return src;
   } else if(url.search("vimeo.com") != -1){
      vid = extract_vimeovid(url);
      src='http://player.vimeo.com/video/'+ vid +'?enablejsapi=1&rel=0&html5=1';
      return src;
   }
   return url;
}

function youtube_embed_code_for_youtb(url, width, height){
    //width=560;height=349;
    yt_embed_plr_id++;
    var src='//www.youtube.com/embed/'+url;
    //var code = "<div class='resp-container' ><iframe class='ytplr' width='" + width + "' height='"+ height +"' id='player" + yt_embed_plr_id +"'  src='"+ src +"' frameborder='0' allowfullscreen></iframe></div>";
    var code = "<div class='resp-container video-container' ><iframe class='ytplr' width='560' height='315' id='player" + yt_embed_plr_id +"'  src='"+ src +"' frameborder='0' allowfullscreen></iframe></div>";
    return code;
}

function swf_embed_code(url){
  var height = $(window).height() * .8;
  width = "100%";
  return '<center><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="'+ width + '" height="'+ height +'" align="middle"> <param name="movie" value="'+  url+'"/><!--[if !IE]>--> <object type="application/x-shockwave-flash" data="'+ url +'" width="'+ width +'" height="' + height +'" > <param name="movie" value="'+ url + '"/> <!--<![endif]--> <a href="http://www.adobe.com/go/getflash"> <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/> </a> <!--[if !IE]>--> </object> <!--<![endif]--> </object></center>';
}
var vimeo_id = 0
function vimeo_embed_code(url, width, height){
   if(matches = url.match(/vimeo.com\/(\d+)/)){
      id = matches[1];
      vid = "vimeo_id_" +id+"_"+ vimeo_id;
      vimeo_id += 1;
      //return '<div class="embed-responsive embed-responsive-16by9"><iframe id="'+vid+'" class="embed-responsive-item vimeo_video" src="https://player.vimeo.com/video/'+ id +'?api=1&player_id='+vid+'&color=ffffff&title=0&byline=0&portrait=0" width="" height="290" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>'
      return '<div class="embed-responsive embed-responsive-16by9 video-container"><iframe id="'+vid+'" class="embed-responsive-item vimeo_video" src="https://player.vimeo.com/video/'+ id +'?api=1&player_id='+vid+'&color=ffffff&title=0&byline=0&portrait=0" width="560" height="" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>'

   }
   return "";
}
function onCreate(player) {
    player.mb.subscribe("*" , 'example', function(eventName) {
        player.mb.subscribe(OO.EVENTS.PLAYED , 'example', function(eventName) {
            video_state = 1;
            console.log('The player has been destroyed!');
        });
    });
}
function audio_end(){
    video_state = 1;
}

function html_embed_enroll_code(url, width, height){
  var height = $(window).height() * .8;
  width = "100%";
  return "<div class='embed-container'><center><iframe frameborder='0' scrolling='yes' width='" + width + "' height='"+ height + "px' src='" + url + "' seamless >Unable to display as iframe is not supported</iframe></center></div>";
}