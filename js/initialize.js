var glob = false;
var ajax_domain;
var logo;

var app = angular.module("mainApp", ['ui.router','ionic','ngCordova']);

//app.constant('$ionicLoadingConfig', {
//  template: 'Default Loading Template...'
//});
//app.config(function($httpProvider) {
//  $httpProvider.interceptors.push(function($rootScope) {
//    return {
//      request: function(config) {
//        $rootScope.$broadcast('loading:show')
//        return config
//      },
//      response: function(response) {
//        $rootScope.$broadcast('loading:hide')
//        return response
//      }
//    }
//  })
//})

angular.module('mainApp').factory('global', function(){
    var assessment_details = {};
	assessment_details['assessment_intro'] = "";
	assessment_details['batch_id'] = "";
	assessment_details['course_title'] = "";
	assessment_details['course_list'] = "";
	assessment_details['assessmentlist'] = "";
	console.log(JSON.stringify(assessment_details['assessment_intro']))
	return assessment_details;
});


angular.module('mainApp').filter('to_trusted', ['$sce', function($sce){
    return function(text) {
        return $sce.trustAsHtml(text);
    };
}]);

angular.module('mainApp').
  filter('htmlToPlaintext', function() {
    return function(t) {
      //return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
      return ($('<div/>').html(t).text());
    };
  }
);

app.directive('onLastRepeat', function() {
    console.log("onlastrepeat directive")
    return function(scope, element, attrs) {
        if (scope.$last) setTimeout(function(){
            scope.$emit('onRepeatLast', element, attrs);
        }, 1);
    };
});

function myFunction(obj) {
    obj.src="js/image/caro6.jpg"
}
function dynamictextimg(obj){
    $("#dyn").addClass("dynimg");
}
function userImage(obj){
    obj.src = "js/image/default_profileimage.gif"
}
function onLoad() {
    document.addEventListener("deviceready", onDeviceReady, false);
}
function defaultUser(obj) {
    obj.src="js/image/caro6.jpg"
}
//function get_embed_code( page,ppwd,ppht){
//  console.log("get_embed_code")
//  ppwd = ppwd
//  ppht = ppht
//  url = page.url;
//  local = page.local;
//  cat = page.ptype;
//  var isLocal = (parseInt(local) == 1);
//  var embedCode = "";
//  //cat  = fixCat(url, cat);
//  if(cat == "2"){
//     if(isLocal){
//        //embedcode = image_embed_code(url, 0, ppht);
//        //embedcode = image_embed_code(url, 0, 0);
//        embedcode = image_embed_code(url, ppwd, ppht);
////        if (url.match(/\.svg/g)) {
////            embedcode = "<center><object type='image/svg+xml' class='img-responsive' style='max-width:" + ppwd +"px; max-height:" + ppht + "px; min-height:" + ppht + "px; min-width:" + ppwd + "px;' data='" + url +"' </></object></center>";
////        }
////        else{
////            embedcode = "<center><div><img class='img-responsive' style='max-width:" + ppwd +"px; max-height:" + ppht + "px; min-height:" + ppht + "px; min-width:" + ppwd + "px;' src='" + url +"' </></div></center>";
////        }
//
//     }
//     else{
//        embedcode = html_embed_code(url, ppwd, ppht);
//     }
//  }
//  else if(cat == "6"){
//     if(isLocal){
//        //embedcode = image_embed_code(url, 0, ppht);
//        //embedcode = image_embed_code(url, 0, 0);
//        embedcode = image_embed_code(url, ppwd, ppht);
//     }
//     else{
//         if(checkimage(url)){
//            //embedcode = image_embed_code(url, 0, ppht);
//            //embedcode = image_embed_code(url, 0, 0);
//            embedcode = image_embed_code(url, ppwd, ppht);
//         }
//         else{
//           embedcode = html_embed_code(url, ppwd, ppht);
//         }
//     }
//  }
//  else if(cat == "4"){
//
//     var vwd = ppwd; var vht = ppht;
//     var page_height = parseInt(ppht);
//     vht = page_height * .8;
//     //vwd = vht * 4/3;
//     if(url.search("ooyala.com") != -1){
//        embedcode = ooyala_js_container(url);
//     }
//     else{
//        if(isLocal){
//           //embedcode = "<div class='html5_locv_wrapper video-container' style='background:black;color:white;cursor:pointer' onclick='javascript:initH5Plr(this);'  init='0' url='" + url + "' ><div style='margin-top:"+ 20+ "%'><center> <h1 style='color:white;'>" + page.ptitle + "</h1><i class='icon-youtube-play icon-round icon-round-lg icon-bg-red'></i></center></div></div>";
//           embedcode = "<div class='video-container' ><video controls width='560' autoplay><source src='" + url + "'  type='video/mp4'>Your browser does not support this video format.</video></div>";
//        }
//        else{
//        embedcode = video_embed_code(url, isLocal, vwd, vht);
//        }
//     }
//  }
//  else if(cat == "9"){
//     var awd = ppwd;
//     if(page.prac_id != ""){
//       awd = .7 * ppwd;
//
//     }
//     embedcode = html5_audio_embed_code(url, awd);
//     //embedcode = "<div class='html5_audio_wrapper' data-src='" + url + "'></div>";
//  }
//  else if(cat == "5"){
//     //embedcode = page.html;
//     embedcode = unescape(page.html);
//  }
//  else{
//     embedcode = html_embed_code(url, ppwd, ppht);
//  }
//  var retcode = "";
//  if(page.prac_id == ""){
//       retcode = embedcode;
//  }
//   else{
//      var ar = "<p>Play the reference clip above and practice. You can record your practice using the OM Riyaaz recorder below. When you are happy with your recording you can save it.</p><div class='recorder'><div onclick='javascript:get_audiorecorder(this);' prac_id='" + page.prac_id +"'><img src='http://s3.amazonaws.com/live.shankarmahadevanacademy.com/portal/images/b_omriyaaz.png'></div></div>";
//      retcode = "<div class='row'><div class='col col-40'><div class='prac_v' style='height:300px;'>" + embedcode + ar+"</div></div>";
//       if(page.prac_ntn_type == "img"){
//          retcode +=  "<div class='col col-60'><div style='overflow-y:auto;overflow-x:hidden;height:" +( page_height - 80) + "px;'><center><img  class='sma_prac_ntn' src='" + page.prac_ntn + "' /></center></div></div></div>";
//       }
//       else{
//          retcode +=  "<div class='col col-60'>" + page.prac_ntn + "</div></div>";
//       }
//   }
//   return retcode
//}
function initH5Plr(el){
   var url = $(el).attr("url");
   var init = $(el).attr("init");
   if(init == "0"){
      var embedcode = "<div class='embed-responsive embed-responsive-16by9' ><video controls class='embed-responsive-item' autoplay><source src='" + url + "'  type='video/mp4'>Your browser does not support this video format.</video></div>";
      $(el).html(embedcode);
      $(el).attr("init", "1");
      $(el).css("background", "transparent");
   }

}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

//new code

function download_save_logo(f_path, response, type){
  		var fileTransfer = new FileTransfer();
  		if(type == 'customer'){
  			//var uri = 'http://shankarmahadevanacademy.kayako.com/__swift/themes/client/images/staffonline.png';
  			var uri = encodeURI('http://'+response['logo']);
  		}
  		else if(type == 'user'){
  			var uri = encodeURI('http:'+response['profile_pic']);
  		}
  		else if(type == 'test'){
  		    var uri = 'http://s3.amazonaws.com/demo.cloodon.com/content/3956.demo.cloodon.com.20160917110145/assets/page-1.svg?AWSAccessKeyId=AKIAJ6U25JMT3GAG6Y7Q&Expires=1477045376&Signature=twyI8VXVoA0gtjwELYDBIODxjcg%3D'
  		}
  		else{

  		}
		var fileURL = f_path;
		fileTransfer.download(
			uri,
			fileURL,
			function(entry) {
				console.log("download complete: " + entry.toURL());
				if(type == 'customer'){
					logo = response['logo'] = entry.toURL();
					//ajax_domain = response['domain_name'] = 'http://www.'+response['domain_name'];
					ajax_domain = response['domain_name'] = 'http://'+response['domain_name'];
					onDeviceReady('customerdata',response,false);
					window.location = "#login";
				}
				else if(type == 'user'){
					response['profile_pic'] = entry.toURL();
					onDeviceReady('usercredential',response,false);
				}
			},
			function(error) {
				console.log("download error source " + error.source);
				console.log("download error target " + error.target);
				console.log("upload error code" + error.code);
			},
			true
//			{
//				headers: {
//					"Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
//				}
//			}
		);
  	}

  	function create_directory(perDir,response,type){
  	    var file_name = '';
  		if(type == 'customer'){
  			file_name = 'customer_log.png';
  		}
  		else if(type == 'user'){
  		    var split_url = response['profile_pic'].split('.');
  		    var file_type = split_url[split_url.length - 1];
  			file_name = 'profile_pic.'+file_type;
  		}
  		else if(type == 'test'){
  		    file_name = 'test.svg';
  		}
  		perDir.getFile(file_name, {create: true, exclusive: false}, function(fileEntry) {
  			download_save_logo(fileEntry.toURL(), response, type)
  			},function(){console.log('error')}
  		);
  	}
//  	function get_audiorecorder(obj){
//  	    console.log(obj);
//  	    obj.remove();
//  	    recorder = new Object;
//  	    $('.recorder').html("<div class='row'><div class='col'><button class='button icon-left ion-mic-a' onclick='start_recording()'>Record</button></div><div class='col'><button class='button icon-left ion-pause' onclick='pause_recording()'>Pause</button></div><div class='col'><button class='button icon-left ion-record' onclick='stop_recording()'>Stop</button></div></div>");
//  	}
//  	function record(){
//  	    $('.recorder').html("<button class='button icon ion-pause-a' onclick='stop_recorder()'></button>");
//  	    recorder.record = function() {
//  window.plugins.audioRecorderAPI.record(function(msg) {
//    // complete
//    alert('ok: ' + msg);
//  }, function(msg) {
//    // failed
//    alert('ko: ' + msg);
//  }, 30); // record 30 seconds
//}
//  	}
//
//  	function stop_recorder(){
//        $('.recorder').html("<button class='button icon ion-mic-off-a' onclick='play_recorder()'></button>");
//        recorder.stop = function() {
//            window.plugins.audioRecorderAPI.stop(function(msg) {
//                // success
//                alert('ok: ' + msg);
//            }, function(msg) {
//                // failed
//                alert('ko: ' + msg);
//            });
//        }
//  	}
//
//    function play_recorder(){
//        $('.recorder').html("<button class='button icon ion-star-a' onclick='play_recorder()'></button>");
//        recorder.playback = function() {
//            window.plugins.audioRecorderAPI.playback(function(msg) {
//                // complete
//                alert('ok: ' + msg);
//            }, function(msg) {
//                // failed
//                alert('ko: ' + msg);
//            });
//        }
//    }
//  	//var recorder = new Object;
//
//recorder.record = function() {
//  window.plugins.audioRecorderAPI.record(function(msg) {
//    // complete
//    alert('ok: ' + msg);
//  }, function(msg) {
//    // failed
//    alert('ko: ' + msg);
//  }, 30); // record 30 seconds
//}
//recorder.playback = function() {
//  window.plugins.audioRecorderAPI.playback(function(msg) {
//    // complete
//    alert('ok: ' + msg);
//  }, function(msg) {
//    // failed
//    alert('ko: ' + msg);
//  });
//}