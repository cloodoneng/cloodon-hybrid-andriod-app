app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
//     if(window.Connection) {
//    if(navigator.connection.type == Connection.NONE) {
//                    $ionicPopup.confirm({
//                        title: "Internet Disconnected",
//                        content: "The internet is disconnected on your device."
//                    })
//                    .then(function(result) {
//                        if(!result) {
//                            ionic.Platform.exitApp();
//                        }
//                    });
//                }
//            }
  });
})
app.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginController'
  })

  // Each tab has its own nav history stack:

  .state('access', {
    url: '/organization',
    templateUrl:'templates/clientlogin.html',
    controller:'CustomerLoginController'
  })

  .state('register', {
    url: '/register',
    templateUrl: 'templates/register.html',
    controller:'RegisterController'
    })

  .state('myhome', {
    url: '/myhome',
    controller:'MyHomeController',
        resolve: {
//            catalog: function(CourseCatalogDetailService) {
//                return CourseCatalogDetailService.get_course_catalog();
//            }
//            usercourse: function(CourseCatalogDetailService){
//                return CourseCatalogDetailService.get_users_course();
//            }
        },
    templateUrl: 'templates/home.html'
  })

  .state('mycourse',{
    url: '/mycourse',
    templateUrl:'templates/courselist.html',
    controller:'CourseListController'
  })
  .state('coursecatalogue', {
    url: '/coursecatalogue',
    controller:'BrowseCourseController',
        resolve: {
            catalog: function(CourseCatalogDetailService) {
                return CourseCatalogDetailService.get_course_catalog();
            }
        },
    templateUrl: 'templates/browse_course.html'
  })
  .state('catalog-course-list',{
    url:'/catalog-course-list/:course_name',
    templateUrl:'templates/catalogcourselist.html',
    controller:'CatalogCourseListController'
  })
  .state('course-detail',{
    url: '/course-detail',
    templateUrl:'templates/coursedetail.html',
    controller:'CourseDetailController'
  })
  .state('course',{
    url:'/course',
    templateUrl:'templates/catalogcoursedetail.html',
    controller:'CatalogCourseController'
  })
  .state('add-discussion',{
    url:'/add-discussion',
    templateUrl:'templates/creatediscussion.html',
    controller:'CreateDiscussionController'
  })
  .state('discussion-detail',{
    url:'/discussion-detail',
    templateUrl:'templates/discussiondetail.html',
    controller:'DiscussionDetailController'
  })
  .state('assessmentintro',{
    url:'/assessmentintro',
    templateUrl: 'templates/assessmentintro.html',
    controller: 'AssessmentIntroController',
  })
  .state('previewassessment',{
    url:'/previewassessment',
    templateUrl: 'templates/previewassessment.html',
    controller: 'PreviewAssessmentController',
  })
  .state('ombook',{
    url:'/ombook/:cid/:pid',
    templateUrl:'templates/ombook.html',
    controller:'OmBookController'
  })
  .state('ombook_start_gate',{
      url:'/ombook_start_gate/:cd',
      templateUrl:'templates/OmbookGate.html',
      controller:'GateOmbookController'
  })
  .state('enroll',{
      url:'/enroll',
      templateUrl:'templates/enrollment.html',
      controller:'EnrollmentController'
  })
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/');

});
//app.run(function ($rootScope,$ionicLoading) {
//});
