app.factory('assessmentservice', ['$http','$q','coursedetailservice',function($http,$q,coursedetailservice){
    var assessment = {};
    assessment.get_assessment = function(id,state,time){
        var deferred = $q.defer();
        var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
        var d;
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM assessmentdetails where qrid =?', [id], function(tx, results) {
                var len = results.rows.length;
                console.log("Assessmentdetails table: " + len + " rows found.");
                for (var i=0; i<len; i++){
                    console.log("Row = " + i + " ID = " + results.rows.item(i).qrid + " Data =  " + results.rows.item(i).response + " Status =  " + results.rows.item(i).status);
                    d = results.rows.item(i).response;
                    var de = {}
                    de['d'] = JSON.parse(d);
                    var state_db = results.rows.item(i).status;
                    de['state_db'] = state_db;
                }
                deferred.resolve(de)
            }, function(err) {
                console.log("Error processing SQL error executesql: "+err.code);
                deferred.reject("error in sql statement")
            });
        }, function (err) {
            console.log("Error processing SQLtransectiondb: "+err.code);
            deferred.reject("data is not their in db")
        });
        return deferred.promise;
    }
    assessment.update_status = function(state,id){
        if (state == 'Due'){
            console.log(coursedetailservice.data)
            angular.forEach(coursedetailservice.data['response']['assessments'], function(value, key) {
                if(value['id'] == id){
                    value['state'] = 'In Progress';
                }
            });
            //coursedetailservice['data']['assessments'] = assessmentlist
            var data = coursedetailservice['data'];
            console.log(JSON.stringify(coursedetailservice.data))
            onDeviceReady('assessmentlists',data,true);
        }
        else{
       }
    }
    assessment.get_json_data = function(defined_data,id,state,time,de,status){
        var deferred = $q.defer();
        if(defined_data == false){
            $http.jsonp(ajax_domain +'/apis/user-assessment-details/?callback=JSON_CALLBACK'+'&id='+id).success(function(response){
                deferred.resolve(response);
                var data = {};
                response['time_limit'] = time;
                console.log(response['time_limit']);
                data['response'] = response;
                data['qrid'] = id;
                data['state'] = state;
                onDeviceReady('assessmentdetails',data,false);
                assessment.update_status(state)
            }).error(function(){
                res = {'status':0,'error_info':'There is no internet connection, Check your internet connection'}
                deferred.reject(res)
            })
        }
        else{
            if(status == 'Evaluated' && state =='Evaluated'){
                var bolstate = true;
            }
            if((state =='Evaluated' || state =='Needs Evaluation' || state =='Due') && (!bolstate)){
                console.log("inside assess details update ")
                $http.jsonp(ajax_domain +'/apis/user-assessment-details/?callback=JSON_CALLBACK'+'&id='+id).success(function(response){
                    deferred.resolve(response);
                    var data = {};
                    response['time_limit'] = time;
                    data['response'] = response;
                    data['qrid'] = id;
                    data['state'] = state;
                    console.log(JSON.stringify(data));
                    onDeviceReady('assessmentdetails',data,true);
                    assessment.update_status(state,id);
                }).error(function(){
                    res = {'status':0,'error_info':'There is no internet connection, Check your internet connection'}
                    deferred.reject(res)
                })
            }
            else{
            }

        }
//        if (state == 'Due'){
//            console.log(coursedetailservice.data)
//            angular.forEach(coursedetailservice.data['response']['assessments'], function(value, key) {
//                if(value['id'] == id){
//                    value['state'] = 'In Progress';
//                }
//            });
//            //coursedetailservice['data']['assessments'] = assessmentlist
//            var data = coursedetailservice['data'];
//            console.log(JSON.stringify(coursedetailservice.data))
//            onDeviceReady('assessmentlists',data,true);
//        }
//        else{
//       }
        return deferred.promise;
    }
    return assessment;
}])