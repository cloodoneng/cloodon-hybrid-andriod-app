app.factory('CourseCatalogDetailService', ['$http','$q',function($http,$q){
    var course_catalog = {};
    course_catalog.get_users_course = function(){
        var deferred = $q.defer();
        $http.jsonp(ajax_domain + '/apis/user-courses/?callback=JSON_CALLBACK').success(function(response){
            var course_info = {};
            if(response['status'] == 1){
                course_info['courses'] = response['courses'];
                course_info['myclasses'] = response['myclasses'];
                course_catalog['users_course'] = course_info;
                //var res = {'status':1,'courses':response['courses']}
                var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
                db.transaction(function(tx) {
                    tx.executeSql('SELECT * FROM courselist where id =1', [], function(tx, results) {
                        onDeviceReady('courselist',response['courses'],true);
                    }, function(err) {
                        console.log("Error processing SQL error executesql: "+err.code);
                        onDeviceReady('courselist',response['courses'],false);
                    });
                }, function (err) {
                    console.log("Error processing SQLtransectiondb: "+err.code);
                    onDeviceReady('courselist',response['courses'],false);
                });
            }
            deferred.resolve(course_catalog['users_course']);
        }).error(function(){
            if(course_catalog['users_course']){
                deferred.resolve(course_catalog['users_course']);
            }
            else{
                var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
                db.transaction(function(tx) {
                    tx.executeSql('SELECT * FROM courselist where id =1', [], function(tx, results) {
                        var len = results.rows.length;
                        var student_courses = [];
                        for (var i=0; i<len; i++){
                            console.log("Row = " + i + " ID = " + results.rows.item(i).id + " Data =  " + results.rows.item(i).course_list);
                            student_courses = results.rows.item(i).course_list;
                        }
                        course_catalog['users_course'] = {}
                        course_catalog['users_course']['courses'] = student_courses;
                        course_catalog['users_course']['myclasses'] = {};
                        //course_catalog['users_course'] = de
                        deferred.resolve(course_catalog['users_course']);
                    }, function(err) {
                        console.log("Error processing SQL error executesql: "+err.code);
                        deferred.reject('Data is not allowed.');
                    });
                }, function (err) {
                    console.log("Error processing SQLtransectiondb: "+err.code);
                    deferred.reject('Data is not allowed.');
                });
            }
        })
        return deferred.promise;
    }
    course_catalog.get_course_catalog = function(){
        var deferred = $q.defer();
        //setTimeout(function() {
        if(course_catalog['data']){
            deferred.resolve(course_catalog['data']);
        }
        else{
            console.log("else")
            $http.jsonp(ajax_domain+'/apis/course-catalog/?callback=JSON_CALLBACK').success(function(response){
                course_catalog['data'] = response;
                if(response['status'] == 1){
                    var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
                    db.transaction(function(tx) {
                        tx.executeSql('SELECT * FROM coursecatalogdetail where id =1', [], function(tx, results) {
                            onDeviceReady('coursecatalogdetail',response,true);
                        }, function(err) {
                            console.log("Error processing SQL error executesql: "+err.code);
                            onDeviceReady('coursecatalogdetail',response,false);
                        });
                    }, function (err) {
                        console.log("Error processing SQLtransectiondb: "+err.code);
                        onDeviceReady('coursecatalogdetail',response,false);
                    });
                }
                deferred.resolve(course_catalog['data']);
            }).error(function(){
                var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
                db.transaction(function(tx) {
                    tx.executeSql('SELECT * FROM coursecatalogdetail', [], function(tx, results) {
                        var len = results.rows.length;
                        for (var i=0; i<len; i++){
                            console.log("Row = " + i + " id = " + results.rows.item(i).id + " data =  " + results.rows.item(i).data);
                            var course_catalog_data = JSON.parse(results.rows.item(i).data);
                        }
                        course_catalog['data'] = course_catalog_data
                        deferred.resolve(course_catalog['data']);
                    }, function(err) {
                        console.log("Error processing SQL error executesql: "+err.code);
                        deferred.reject('Data is not allowed.');
                    });
                }, function (err) {
                    console.log("Error processing SQLtransectiondb: "+err.code);
                    deferred.reject('Data is not allowed.');
                });
            })
        }
        //}, 3000);
        return deferred.promise;
    }
    return course_catalog;
}])