app.factory('coursedetailservice', ['$http','$q',function($http,$q){
    var course_detail = {};
    course_detail['discussions'] = {};
    course_detail.ombook_data = {};
    course_detail.get_course_detail_callback = function(batch_id){
        var promise = course_detail.get_course_detail(batch_id)
        promise.then(function(data) {
            //alert('Success: ' + JSON.stringify(data));
            console.log(JSON.stringify(data))
        }, function(reason) {
            alert('Failed: ' + reason);
        }, function(update) {
            alert('Got notification: ' + update);
        });
    }
    course_detail.get_discussion = function(batch_id,d_id){
        var deferred = $q.defer();
        var url;
        if(d_id){
            url = ajax_domain+'/apis/discussions/?callback=JSON_CALLBACK'+'&batch='+batch_id+'&n=5'+'&thread='+d_id
        }
        else{
            url = ajax_domain+'/apis/discussions/?callback=JSON_CALLBACK'+'&batch='+batch_id+'&n=5'
        }
        $http.jsonp(url).success(function(response){
            console.log("test")
            deferred.resolve(response);
            console.log("test after resolve");
        }).error(function(){
            deferred.reject("Check Internet Connectivity")
        })
        return deferred.promise;
    }
    course_detail.get_course_detail = function(batch_id){
        var deferred = $q.defer();
        $http.jsonp(ajax_domain+'/apis/course-details/'+batch_id+'?callback=JSON_CALLBACK').success(function(response){
            var data = {}
            data['batch_id'] = batch_id;
            data['response'] = response
            if(response['status'] == 1){
                course_detail['data'] = data;
                var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
                db.transaction(function(tx) {
                    tx.executeSql('SELECT * FROM coursedetail', [], function(tx, results) {
                        onDeviceReady('coursedetail',data,true);
                    }, function(err) {
                        console.log("Error processing SQL error executesql: "+err.code);
                        onDeviceReady('coursedetail',data,false);
                    });
                }, function (err) {
                    console.log("Error processing SQLtransectiondb: "+err.code);
                    onDeviceReady('coursedetail',data,false);
                });
                deferred.resolve(course_detail['data']);
            }
        }).error(function(){
            var b_id;
            if(course_detail['data']){
                b_id = course_detail['data']['batch_id'];
            }
            else{
                console.log("first else")
            }
            if(course_detail['data'] && (b_id == batch_id)){
                deferred.resolve(course_detail['data']);
            }
            else{
                var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
                var model_data;
                db.transaction(function(tx) {
                    tx.executeSql('SELECT * FROM coursedetail  WHERE batch_id = ?', [batch_id], function(tx, results) {
                        var len = results.rows.length;
                        console.log("coursedetail table: " + len + " rows found.");
                        for (var i=0; i<len; i++){
                            console.log("Row = " + i + " ID = " + results.rows.item(i).batch_id + " Data =  " + results.rows.item(i).response);
                            d = results.rows.item(i).response;
                            var de = {};
                            de['batch_id'] = results.rows.item(i).batch_id
                            de['response'] = JSON.parse(d)
                            model_data = de;
                        }
                        course_detail['data'] = model_data;
                        deferred.resolve(course_detail['data']);
                    }, function(err) {
                        console.log("Error processing SQL error executesql: "+err.code);
                        deferred.reject('Data is not allowed.');
                    });
                }, function (err) {
                    console.log("Error processing SQLtransectiondb: "+err.code);
                    deferred.reject('Data is not allowed.');
                });
            }
        });
        return deferred.promise;
    }
    return course_detail;
}]);