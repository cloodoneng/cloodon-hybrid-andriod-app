app.factory('logindetailservice', ['$http','$q','$rootScope','$ionicLoading',function($http,$q,$rootScope,$ionicLoading){
    var login_updates = {};
    login_updates.get_login_credential = function(){
        var deferred = $q.defer();
        var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM customerdata', [], function(tx, results) {
                var len = results.rows.length;
                var logging_data = {};
                for (var i=0; i<len; i++){
                    console.log("Row = " + i + " id = " + results.rows.item(i).id + " data =  " + results.rows.item(i).customer_json);
                    logging_data['customer_data'] = results.rows.item(i).customer_json;
                }
                tx.executeSql('SELECT * FROM usercredential', [], function(tx, results) {
                	var len = results.rows.length;
                	for (var i=0; i<len; i++){
                    	console.log("Row = " + i + " id = " + results.rows.item(i).id + " data =  " + results.rows.item(i).data);
                    	logging_data['users_credential'] = results.rows.item(i).data;
                    	deferred.resolve(logging_data);
                	}
                }, function(err){
                	logging_data['users_credential'] = JSON.stringify({});
                	deferred.resolve(logging_data);
                });
            }, function(err) {
                console.log("Error processing SQL error executesql: "+err.code);
                deferred.reject("data not found")
            });
        }, function (err) {
            console.log("Error processing SQLtransectiondb: "+err.code);
            deferred.reject("database is not avliable")
        });
        return deferred.promise;
    }

    login_updates.send_user_credentail = function(user_credential, login_view){
    	$rootScope.login_error_msg = '';
    	$ionicLoading.show({content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
        $.ajax({
      	    type: 'POST',
      		url:ajax_domain + '/apis/login/',
      		crossDomain: true,
       		data: user_credential,
       		dataType: 'json',
       		success: function(response, textStatus, jqXHR) {
       			$ionicLoading.hide();
       			console.log(JSON.stringify(response))
   				if(response['status'] == 1){
   					$rootScope.session_id = response['session_id'];
   					response['username'] = user_credential['username'];
   					response['password'] = user_credential['password'];
   					document.addEventListener("deviceready", function() {
       					window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs){
       						create_directory(fs.root,response,'user');
						}, function(){
							console.log('ERROR')
						});
					}, false);
   					$rootScope.user = response
   					console.log($rootScope.user)
    				window.location = "#myhome";
    			}
    			else{
    				$rootScope.login_error_msg = response['error_description'];
    			    if(login_view == false){
    		            window.location = "#login";
    		        }
    		        else{
    		        	$("#login_error").html($rootScope.login_error_msg);
    		        }
//    			    var err = $("#errorappend");
//    				if((err.length)>0){
//    					$("#errorappend").remove();
//    				}
//    				var message = '<div class="list" id="errorappend"><div class="item assertive">'+response['error_description']+'</div></div>';
//    				$("#error").append(message);
    			}
        	},
        	error: function (responseData, textStatus, errorThrown) {
        		$ionicLoading.hide();
            	if(login_view == false){
//            		$rootScope.user = user_credential;
            		window.location = "#myhome";
            	}
            	else{
            		$rootScope.login_error_msg = 'Check internet connectivity.';
            		$("#login_error").html($rootScope.login_error_msg);
            		//window.location = "#login";
            	}
            },
            complete: function(jqXHR, Status){
            	console.log("complete call");
            }
  		});
    }
    return login_updates;
}]);