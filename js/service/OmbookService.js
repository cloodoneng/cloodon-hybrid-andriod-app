app.factory('ombookservice', ['$http','$q',function($http,$q, $window){
    var ombook = {}
    ombook['ombook_details'] = {};
    ombook.get_ombook = function(batch_id,ombook_id,version){
        var deferred = $q.defer();
        var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM ombookdetails  WHERE ombook_id = ?', [ombook_id], function(tx, results) {
                var len = results.rows.length;
                console.log("coursedetail table: " + len + " rows found.");
                if(len > 0){
                for (var i=0; i<len; i++){
                    console.log("Row = " + i + " ID = " + results.rows.item(i).ombook_id + "version = " +results.rows.item(i).version + " Data =  " + results.rows.item(i).response );
                    var ver = results.rows.item(i).version;
                    console.log(version)
                    console.log(ver)
                    if(version == ver){
                        console.log("version matched")
                        ombook['ombook_details'] = JSON.parse(results.rows.item(i).response);
                        deferred.resolve(ombook['ombook_details']);
                    }
                    else{
                        console.log("version didn't match")
                        url = ajax_domain+'/apis/ombook/?callback=JSON_CALLBACK'+'&batch='+batch_id+'&omb='+ombook_id;
                        $http.jsonp(url).success(function(response){
                            console.log("test")
                            //new
                            ombook['ombook_details'] = response;
                            deferred.resolve(response);
                            console.log("test after resolve");
                            var data = {};
                            data['version'] = version;
                            data['ombook_id'] = ombook_id;
                            data['response'] = response;
                            onDeviceReady('ombookdetails',data,true);
                        }).error(function(){
                            deferred.reject("Check Internet Connectivity")
                        })
                    }
                }
                }
                else{
                     url = ajax_domain+'/apis/ombook/?callback=JSON_CALLBACK'+'&batch='+batch_id+'&omb='+ombook_id;
                    $http.jsonp(url).success(function(response){
                        console.log("test")
                        //new
                        ombook['ombook_details'] = response;
                        deferred.resolve(response);
                        console.log("test after resolve");
                        var data = {};
                        data['version'] = version;
                        data['ombook_id'] = ombook_id;
                        data['response'] = response;
                        onDeviceReady('ombookdetails',data,false);
                    }).error(function(){
                        deferred.reject("Check Internet Connectivity")
                    })
                }
            }, function(err) {
                console.log('sql error')
                console.log("Error processing SQL error executesql: "+err.code);
                url = ajax_domain+'/apis/ombook/?callback=JSON_CALLBACK'+'&batch='+batch_id+'&omb='+ombook_id;
                $http.jsonp(url).success(function(response){
                    console.log("test")
                    //new
                    ombook['ombook_details'] = response;
                    deferred.resolve(response);
                    console.log("test after resolve");
                    var data = {};
                    data['version'] = version;
                    data['ombook_id'] = ombook_id;
                    data['response'] = response;
                    onDeviceReady('ombookdetails',data,false);
                }).error(function(){
                    deferred.reject("Check Internet Connectivity")
                })
            });
        }, function (err) {
            console.log('db doesnt exists')
            console.log("Error processing SQLtransectiondb: "+err.code);
            url = ajax_domain+'/apis/ombook/?callback=JSON_CALLBACK'+'&batch='+batch_id+'&omb='+ombook_id;
            $http.jsonp(url).success(function(response){
                console.log("test")
                //new
                ombook['ombook_details'] = response;
                deferred.resolve(response);
                console.log("test after resolve");
                var data = {};
                data['version'] = version;
                data['ombook_id'] = ombook_id;
                data['response'] = response;
                onDeviceReady('ombookdetails',data,false);
            }).error(function(){
                deferred.reject("Check Internet Connectivity")
            })
        });
        return deferred.promise;
    }
    ombook.update_ombook_progress = function(progress_update){
        $.ajax({
            type: "POST",
            url: ajax_domain +'/apis/ombook/update-progress/',
            data: {'data':progress_update},
            dataType:'json',
            timeout : 70000,
            error : function(){
                console.log("failed")
                db_list  = JSON.parse(progress_update);
                console.log(db_list)
                progress_store = progress_store.concat(db_list);
                console.log(progress_store)
            },
            success : function(response){
                console.log(JSON.stringify(response))
            }
        })
    }
    return ombook;
}])
